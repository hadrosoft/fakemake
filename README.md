# FakeMake
#### by Clarissa Toney

*If you can't make it... fake it!*

FakeMake is an incredibly rudimentary tool for compiling source files. Written because I don't know `make`, and Open Watcom (the compiler of choice for my current project) doesn't really have a decent IDE. I really should learn `make` like a good little developer, but it's archaic and frustrating, and I'm out of patience. I'll get back to it eventually, but for now, all I want to do is code.

FakeMake is provided under the [MIT license](LICENSE.md) &mdash; although I doubt anybody would ever actually want to use it. It's basically designed to my own, very specific needs. But hey, if you like it, have at it.

### Usage
From your project directory, type `fakemake` (you may need to add Fakemake's location to your PATH). FakeMake will look for a file by the name `Fakefile` (case-insensitive). An example `Fakefile` is provided in this repository.

Additional command line options can be found by typing `fakemake --help`.

### Requirements
- *To build:* a recent version of GCC

- *To use:*
    - [Open Watcom v1.9](http://www.openwatcom.org) or [v2.0](https://github.com/open-watcom/open-watcom-v2/releases) (probably works with earlier versions)
    - GCC. See [this issue](#1) for details.
    - Ubuntu Linux, although it should work on other operating systems assuming you have Open Watcom and your environment variables are set correctly.
