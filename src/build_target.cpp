#include "build_target.h"

#include <iostream>

#include <core/common.h>
#include <core/string.h>
#include <core/file_system.h>
#include <core/file_directory.h>

#include "settings.h"
#include "system_meta.h"
#include "terminal_color.h"

std::vector<BuildTarget> activeBuildTargets;

/*
========================================
*/

BuildTarget* getBuildTarget (const std::string& targetName)
{
    for (BuildTarget& check: activeBuildTargets) {
        if (check.name == targetName) { return &check; }
    }
    return nullptr;
}

//------------------------------------------------------------------------------

/*
========================================
*/

void BuildTarget::parse()
{
    for (size_t index = 0; index < buildLines.size(); ++index) {
        const std::string& line  = buildLines[index];
        const std::string  first = lcase(string_word(line, 1));

        if (first == "exe_filename") {
            getExeFilename(index);
        }
        else if (first == "source_types") {
            getSourceFileExtensions(index);
        }
        else if (first == "source_directories") {
            getSourceDirectories(index);
        }
        else if (first == "source_files") {
            getExtraSourceFiles(index);
        }
        else if (first == "exclude_files") {
            getExcludeSourceFiles(index);
        }
        else if (first == "include_directories") {
            getIncludeDirectories(index);
        }
        else if (first == "obj_directory") {
            getObjDirectory(index);
        }
        else if (first == "compiler_switches") {
            getCompilerSwitches(index);
        }
        else if (first == "per_file_switches") {
            getPerFileSwitches(index);
        }
        else if (first == "defines") {
            getDefines(index);
        }
        else if (first == "linker_switches") {
            getLinkerSwitches(index);
        }
        else if (first == "linker_library_directories") {
            getLinkerLibraryDirectories(index);
        }
        else if (first == "linker_libraries") {
            getLinkerLibraries(index);
        }
    }
}

/*
========================================
*/

void BuildTarget::getExeFilename (size_t& index)
{
    const std::string& line   = buildLines[index];
    const std::string  second = string_word(line, 2);

    if (!second.empty()) {
        if (second.front() == '"') {
            exeFilename = read_quoted_string(line, 2).first;
        }
        else {
            exeFilename = second;
        }
    }
}

/*
========================================
*/

void BuildTarget::getSourceFileExtensions (size_t& index)
{
    for (index++; index < buildLines.size(); ++index) {
        const std::string line  = buildLines[index];
        const std::string first = lcase(string_word(line, 1));
        if (first == "end") {
            return;
        }
        else {
            sourceTypes.push_back(first);
            while (!sourceTypes.back().empty())
            {
                if (sourceTypes.back().front() == '*' ||
                    sourceTypes.back().front() == '.')
                {
                    sourceTypes.back().erase(0, 1);
                }
                else {
                    break;
                }
            }
        }
    }
}

/*
========================================
*/

void BuildTarget::getSourceDirectories (size_t& index)
{
    for (index++; index < buildLines.size(); ++index) {
        const std::string line   = buildLines[index];
        const std::string first  = string_word(line, 1);
        const std::string second = string_word(line, 2);
        if (lcase(first) == "end") {
            return;
        }
        else {
            SourceDir dir;
            if (first == "(recursive)") {
                if (!second.empty()) {
                    sourceDirectories.push_back({
                        true,
                        (second.front() == '"') ? read_quoted_string(line, 2).first : second});
                }
            }
            else {
                sourceDirectories.push_back({
                    false,
                    (first.front() == '"') ? read_quoted_string(line, 1).first : first});
            }
        }
    }
}

/*
========================================
*/

void BuildTarget::getExtraSourceFiles (size_t& index)
{
    for (index++; index < buildLines.size(); ++index) {
        const std::string line   = buildLines[index];
        const std::string first  = string_word(line, 1);
        if (lcase(first) == "end") {
            return;
        }
        else {
            if (first.front() == '"') {
                extraSourceFiles.push_back(read_quoted_string(line, 1).first);
            }
            else {
                extraSourceFiles.push_back(first);
            }

            #ifdef __linux__
            std::string& filename = extraSourceFiles.back();
            if (filename.front() == '~') {
                filename = canonicalized_path(get_home_directory() + "/" + safe_substr(filename, 1));
            }
            #endif
        }
    }
}

/*
========================================
*/

void BuildTarget::getExcludeSourceFiles (size_t& index)
{
    for (index++; index < buildLines.size(); ++index) {
        const std::string line  = buildLines[index];
        const std::string first = string_word(line, 1);
        if (lcase(first) == "end") {
            return;
        }
        else {
            if (first.front() == '"') {
                excludeSourceFiles.push_back(read_quoted_string(line, 1).first);
            }
            else {
                excludeSourceFiles.push_back(first);
            }
        }
    }
}

/*
========================================
*/

void BuildTarget::getIncludeDirectories (size_t& index)
{
    for (index++; index < buildLines.size(); ++index) {
        const std::string line  = buildLines[index];
        const std::string first = string_word(line, 1);
        if (lcase(first) == "end") {
            return;
        }
        else {
            if (first.front() == '"') {
                includeDirectories.push_back(read_quoted_string(line, 1).first);
            }
            else {
                includeDirectories.push_back(first);
            }
        }
    }
}

/*
========================================
*/

void BuildTarget::getObjDirectory (size_t& index)
{
    const std::string& line   = buildLines[index];
    const std::string  second = string_word(line, 2);

    if (!second.empty()) {
        if (second.front() == '"') {
            objDirectory = read_quoted_string(line, 2).first;
        }
        else {
            objDirectory = second;
        }
    }
}

/*
========================================
*/

void BuildTarget::getCompilerSwitches (size_t& index)
{
    for (index++; index < buildLines.size(); ++index) {
        const std::string line  = buildLines[index];
        const std::string first = string_word(line, 1);
        if (lcase(first) == "end") {
            return;
        }
        else {
            const std::string switchStr = safe_substr(line, string_word_char_pos(line, 1));
            /*  Ignore the following:
                -d:   Fakefile has a special section for defines
                -fr=: FakeMake automatically places error files in obj directory    */
            //  NOTE -- the first condition is commented out because it hides
            //          debug switches (d1, d2 etc.)
            if (
                /*safe_substr(switchStr, 0, 2) != "-d" &&*/
                safe_substr(switchStr, 0, 4) != "-fr=")
            {
                compilerSwitches.push_back(switchStr);
            }
        }
    }
}

/*
========================================
*/

void BuildTarget::getPerFileSwitches (size_t& index)
{
    std::vector<std::string> files;
    std::vector<std::string> switches;

    for (index++; index < buildLines.size(); ++index) {
        const std::string line   = buildLines[index];
        const std::string first  = string_word(line, 1);
        const std::string second = string_word(line, 2);
        if (lcase(first) == "end") {
            break;
        }
        else {
            if (lcase(first) == "file:") {
                if (first.front() == '"') {
                    files.push_back(read_quoted_string(line, 2).first);
                }
                else {
                    files.push_back(second);
                }
            }
            else {
                const std::string switchStr = safe_substr(line, string_word_char_pos(line, 1));
                /*  Ignore the following:
                    -d:   Fakefile has a special section for defines
                    -fr=: FakeMake automatically places error files in obj directory    */
                //  NOTE -- the first condition is commented out because it hides
                //          debug switches (d1, d2 etc.)
                if (
                    /*safe_substr(switchStr, 0, 2) != "-d" &&*/
                    safe_substr(switchStr, 0, 4) != "-fr=")
                {
                    switches.push_back(switchStr);
                }
            }
        }
    }

    if (switches.empty()) { return; }
    for (const std::string& str: files) {
        fileSwitches.push_back({str, switches});
    }
}

/*
========================================
*/

void BuildTarget::getDefines (size_t& index)
{
    for (index++; index < buildLines.size(); ++index) {
        const std::string line  = buildLines[index];
        const std::string first = string_word(line, 1);
        if (lcase(first) == "end") {
            return;
        }
        else {
            for (int wordId = 1;; ++wordId) {
                const std::string token = string_word(line, wordId);
                if (token.empty()) { break; }
                defines.push_back(token);
            }
        }
    }
}

/*
========================================
*/

void BuildTarget::getLinkerSwitches (size_t& index)
{
    for (index++; index < buildLines.size(); ++index) {
        const std::string line  = buildLines[index];
        const std::string first = string_word(line, 1);
        if (lcase(first) == "end") {
            return;
        }
        else {
            linkerSwitches.push_back(safe_substr(line, string_word_char_pos(line, 1)));
        }
    }
}

/*
========================================
*/

void BuildTarget::getLinkerLibraryDirectories (size_t& index)
{
    for (index++; index < buildLines.size(); ++index) {
        const std::string line  = buildLines[index];
        const std::string first = string_word(line, 1);
        if (lcase(first) == "end") {
            return;
        }
        else {
            if (first.front() == '"') {
                linkerLibraryDirectories.push_back(read_quoted_string(line, 1).first);
            }
            else {
                linkerLibraryDirectories.push_back(first);
            }
        }
    }
}

/*
========================================
*/

void BuildTarget::getLinkerLibraries (size_t& index)
{
    for (index++; index < buildLines.size(); ++index) {
        const std::string line  = buildLines[index];
        const std::string first = string_word(line, 1);
        if (lcase(first) == "end") {
            return;
        }
        else {
            if (first.front() == '"') {
                linkerLibraries.push_back(read_quoted_string(line, 1).first);
            }
            else {
                linkerLibraries.push_back(first);
            }
        }
    }
}

/*
========================================
*/

void BuildTarget::fixPaths()
{
    fixPath(exeFilename);
    for (SourceDir& dir: sourceDirectories) { fixPath(dir.directory); }
    for (std::string& str: extraSourceFiles) { fixPath(str); }
    for (std::string& str: excludeSourceFiles) { fixPath(str); }
    for (std::string& str: includeDirectories) { fixPath(str); }
    fixPath(objDirectory);
    for (std::string& str: linkerLibraryDirectories) { fixPath(str); }
    for (std::string& str: linkerLibraries) { fixPath(str); }
    for (FileSwitch& opt: fileSwitches) { fixPath(opt.filename); }
}

/*
========================================
*/

void BuildTarget::compileFinalSourceFileList()
{
    allSourceFiles.clear();

    for (const SourceDir& dir: sourceDirectories) {
        for (const std::string& ext: sourceTypes) {
            const directory_listing listing(dir.directory, "*." + ext);
            const std::vector<std::string> fileListing =
                (dir.recursive) ? listing.full_recursive_file_list(false) :
                                  listing.full_file_list(false);
            for (const std::string& filename: fileListing) {
                allSourceFiles.push_back({filename, ""});
            }
        }
    }

    for (const std::string& filename: extraSourceFiles) {
        allSourceFiles.push_back({filename, ""});
    }

    for (auto it = allSourceFiles.begin(); it != allSourceFiles.end();) {
        const SourceObjPair& entry = *it;
        if (vector_contains_value(excludeSourceFiles, entry.sourceFile) ||
            !file_exists(entry.sourceFile) ||
            file_type(entry.sourceFile) != File_Type::File)
        {
            it = allSourceFiles.erase(it);
        }
        else {
            ++it;
        }
    }
}

/*
========================================
*/

void BuildTarget::generateObjectFilePaths()
{
    for (SourceObjPair& entry: allSourceFiles) {
        if (safe_substr(entry.sourceFile, 0, cwd.size()) == cwd) {
            entry.objectFile = objDirectory + "/" + safe_substr(entry.sourceFile, cwd.size());
        }
        else {
            entry.objectFile = objDirectory + "/out/" + entry.sourceFile;
        }
        entry.objectFile = strip_file_extension(entry.objectFile) + ".obj";
        entry.objectFile = canonicalized_path(entry.objectFile);
    }
}

/*
========================================
*/

bool BuildTarget::anyIncludeChanged (const SourceObjPair& sourceFile)
{
    if (!file_exists(sourceFile.objectFile)) { return false; }
    if (!file_exists(sourceFile.sourceFile)) { return false; }

    std::string command = "gcc -M -I $INCLUDE ";
    for (const std::string& str: includeDirectories) { command += "-I '" + str + "' "; }
    for (const std::string& str: defines) { command += "-D" + str + " "; }
    command += " '" + sourceFile.sourceFile + "'";

    SystemResult cmd = getOutputOfSystemCommand(command, false);

    std::vector<std::string> vec;
    for (std::string tempString: cmd.output) {
        tempString       = trim(tempString, " \t\n\\") + " ";
        size_t lastFirst = 0;
        for (size_t index = 0; index < tempString.size(); ++index) {
            if (tempString[index] == ' ' &&
                (index == 0 || tempString[index - 1] != '\\'))
            {
                vec.push_back(trim(safe_substr(tempString, lastFirst, index - lastFirst + 1)));
                string_replace(vec.back(), "\\ ", " ");
                fixPath(vec.back());
                lastFirst = index;
            }
        }
    }

    if (!vec.empty()) { vec.erase(vec.begin()); }       //  Erase "object" filename
    if (!vec.empty()) { vec.erase(vec.begin()); }       //  Do it again -- erase "source" filename

    const time_t sourceTime = fileModifiedTimestamp(sourceFile.objectFile);
    for (const std::string& filename: vec) {
        if (!file_exists(filename)) { continue; }
        if (fileModifiedTimestamp(filename) > sourceTime) { return true; }
    }

    return false;
}

/*
========================================
*/

std::vector<std::string>
BuildTarget::uniqueSwitchesForFile (const std::string& filename)
{
    for (const FileSwitch& opt: fileSwitches) {
        if (canonicalized_path(opt.filename) == canonicalized_path(filename)) {
            return opt.switches;
        }
    }
    return {};
}

/*
========================================
*/

void BuildTarget::clean()
{
    std::cout <<
        "------ Clean: " << name << " in " << extract_filename(exeFilename) <<
        " (compiler: " << extract_filename(compiler) << ") ------" << std::endl;

    if (!directory_exists(objDirectory)) { return; }        //  Already clean

    const directory_listing listing(objDirectory, "*.*", true);
    const std::vector<std::string> fullList = listing.full_recursive_file_list(true);

    //  Delete all files
    for (const std::string& filename: fullList) {
        if (filename != objDirectory &&
            file_type(filename) == File_Type::File)
        {
            kill_file(filename);
        }
    }

    //  Remove anything that's left (directories)
    for (const std::string& filename: fullList) {
        if (filename != objDirectory) { kill_file(filename); }
    }

    std::cout << "Cleaned \"" << extract_filename(exeFilename) << " - " << name << "\"" << std::endl << std::endl;
}

/*
========================================
*/

bool BuildTarget::compile (const bool fromClean)
{
    if (fromClean) { clean(); }

    std::cout <<
        "------ Build: " << name << " in " << extract_filename(exeFilename) <<
        " (compiler: " << extract_filename(compiler) << ") ------" << std::endl;

    actualBuildList.clear();

    //  Run through list to get percentages
    for (const SourceObjPair& entry: allSourceFiles) {
        if (!file_exists(entry.sourceFile)) {
            std::cout << "Error: source file '" << entry.sourceFile << "' does not exist" << std::endl;
            return false;
        }
        const time_t sourceTimestamp = fileModifiedTimestamp(entry.sourceFile);
        if (!file_exists(entry.objectFile) ||
            fileModifiedTimestamp(entry.objectFile) < sourceTimestamp ||
            anyIncludeChanged(entry))
        {
            const std::string objectDirectory = get_directory(entry.objectFile);
            if (!directory_exists(objectDirectory, true)) {
                std::cout <<
                    "Error: object target directory '" << objectDirectory << "' "
                    "does not exist and could not be created" << std::endl;
                return false;
            }

            actualBuildList.push_back({entry.sourceFile, entry.objectFile});
        }
    }

    const int   totalSteps = actualBuildList.size() + 1;
    const float stepPct    = 100.0f / totalSteps;
    float       progress   = 0.0f;

    //  Build files
    for (const SourceObjPair& entry: actualBuildList) {
        const std::string errFilename = strip_file_extension(entry.objectFile) + ".err";
        std::string       command     = compiler + " -q ";
        for (const std::string& str: includeDirectories) { command += " -i='" + str + "' "; }
        for (const std::string& str: compilerSwitches) { command += str + " "; }
        for (const std::string& str: globalCompilerSwitches) { command += str + " "; }
        for (const std::string& str: uniqueSwitchesForFile(entry.sourceFile)) { command += str + " "; }
        for (const std::string& str: defines) { command += "-d" + str + " "; }
        for (const std::string& str: globalDefines) { command += "-d" + str + " "; }
        command += "-fo='" + entry.objectFile + "' ";
        command += "-fr='" + errFilename + "' ";
        command += "'" + entry.sourceFile + "'";

        progress += stepPct;
        std::cout << FG_GREEN << "[" << percentageLabel(progress) << "%] " << FG_DEFAULT << "Compiling: " << extract_filename(entry.sourceFile) << std::endl;
        kill_file(entry.objectFile);
        kill_file(errFilename);

        //  Output messages with color
        const SystemResult cmd = getOutputOfSystemCommand(command, true);
        for (std::string str: cmd.output) {
            str = trim(str, "\n");
            const char        firstChar  = str.front();
            const std::string firstWord  = string_word(str, 1);
            const std::string secondWord = string_word(str, 2);
            if (firstWord == "Warning!") {
                std::cout << FG_YELLOW << str << FG_DEFAULT << std::endl;
            }
            else if (secondWord == "Warning!") {
                std::cout << FG_YELLOW << str << FG_DEFAULT << std::endl;
            }
            else if (firstChar == '(' && secondWord == "Warning!") {
                std::cout << FG_YELLOW << str << FG_DEFAULT << std::endl;
            }
            else if (firstWord == "Error!" ||
                     (firstChar == '(' && secondWord == "Error!"))
            {
                std::cout << FG_RED << str << FG_DEFAULT << std::endl;
            }
            else if (secondWord == "Error!" &&
                     safe_substr(str, 0, entry.sourceFile.size()) == entry.sourceFile)
            {
                std::cout << FG_RED << str << FG_DEFAULT << std::endl;
            }
            else if (secondWord == "Error!") {
                std::cout << FG_RED << str << FG_DEFAULT << std::endl;
            }
            else if (secondWord == "Note!") {
                std::cout << FG_YELLOW << str << FG_DEFAULT << std::endl;
            }
            else {
                std::cout << trim(str) << std::endl;
            }
        }
        if (!cmd.output.empty()) { std::cout << std::endl; }
        if (cmd.result != 0) { return false; }
    }

    return true;
}

/*
========================================
*/

bool BuildTarget::link()
{
    if (actualBuildList.empty() && file_exists(exeFilename)) {
        std::cout << "Target is up to date." << std::endl;
        std::cout << "Nothing to be done (all items are up-to-date)." << std::endl;
        doNothing = true;
        return true;
    }

    std::cout << FG_GREEN << "[100.0%] " << FG_DEFAULT << "Linking executable: " << extract_filename(exeFilename) << std::endl;

    std::string command    = linker + " ";
    bool        debugFound = false;

    //  Parse project linker switches
    for (const std::string& str: linkerSwitches) {
        if (lcase(string_word(str, 1)) == "debug") {
            command += str + " ";
            debugFound = true;
        }
    }
    command += " name '" + exeFilename + "' op q ";
    for (const std::string& str: linkerSwitches) {
        if (lcase(string_word(str, 1)) != "debug") {
            command += str + " ";
        }
    }

    //  Parse global linker switches
    for (const std::string& str: globalLinkerSwitches) {
        if (lcase(string_word(str, 1)) == "debug") {
            if (!debugFound) {
                command = str + " " + command;
            }
        }
        else {
            command += str + " ";
        }
    }

    if (!linkerLibraryDirectories.empty()) {
        command += "libp ";
        for (const std::string& str: linkerLibraryDirectories) {
            command += "'" + str + "';";
        }
        if (command.back() == ';') { command.pop_back(); }
        command += " ";
    }

    if (!linkerLibraries.empty()) {
        command += "libr ";
        for (const std::string& str: linkerLibraries) {
            command += "'" + str + "',";
        }
        if (command.back() == ',') { command.pop_back(); }
        command += " ";
    }

    command += "file ";
    for (const SourceObjPair& entry: allSourceFiles) {
        command += "'" + entry.objectFile + "',";
    }
    if (command.back() == ',') { command.pop_back(); }

    const SystemResult cmd = getOutputOfSystemCommand(command, true);

    for (std::string str: cmd.output) {
        str = trim(str, "\n");
        const std::string firstWord  = string_word(str, 1);
        const std::string secondWord = string_word(str, 2);
        if (secondWord == "Warning!") {
            std::cout << FG_YELLOW << str << FG_DEFAULT << std::endl;
        }
        else if (firstWord == "Error!") {
            std::cout << FG_RED << str << FG_DEFAULT << std::endl;
        }
        else if (firstWord == "file" &&
                 string_contains_substring(str, "undefined symbol"))
        {
            for (size_t index = 0; index < str.size(); ++index) {
                if (index >= 4 && safe_substr(str, index - 4, 4) == ".obj") {
                    std::cout << safe_substr(str, index) << std::endl;
                }
            }
        }
        else {
            std::cout << trim(str) << std::endl;
        }
    }

    if (cmd.result == 0) {
        std::cout << "Output file is " << exeFilename << " with size ";

        const size_t bytes = file_size(exeFilename);
        if (bytes < 1024 * 1024) {
            std::cout << numstr(bytes / 1024.0f, 1) << " KiB" << std::endl;
        }
        else {
            std::cout << numstr(bytes / 1024.0f / 1024.0f, 1) << " MiB" << std::endl;
        }
        return true;
    }

    return false;
}
