#include "settings.h"

const std::string VERSION_NUMBER = "1.13";

#ifdef __linux__
bool color = true;
#endif
bool                     version         = false;
std::string              newTarget;
bool                     help            = false;
bool                     clean           = false;
bool                     quiet           = false;
bool                     dryRun          = false;
bool                     dryRunAll       = false;
bool                     doNothing       = false;
bool                     buildAllTargets = false;

std::string              cwd;
std::string              makefilename  = "Fakefile";
std::string              makefilepath;
std::string              defaultTarget = "Release";
std::vector<std::string> preBuildCommands;
std::vector<std::string> postBuildCommands;
std::vector<std::string> globalDefines;
std::vector<std::string> globalCompilerSwitches;
std::vector<std::string> globalLinkerSwitches;
time_t                   startTime;
std::string              compiler;
std::string              linker;
std::vector<std::string> specifiedBuildNames;   //  Target names from command line
