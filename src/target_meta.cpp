#include "target_meta.h"

#include <core/file_system.h>

#include "settings.h"

/*
========================================
*/

void fixPath (std::string& path)
{
    if (path.empty()) { return; }
    //  If no slash at the beginning, assume relative to working directory
    if (path.front() != '/' && path.front() != '\\') {
        path = canonicalized_path(cwd + "/" + path);
    }
    else {
        path = canonicalized_path(path);
    }
}
