#ifndef SYSTEM_META_H_INCLUDED
#define SYSTEM_META_H_INCLUDED

#include <vector>
#include <string>

struct ErrorResult
{
    bool result;
    int  errorCode;
};

struct SystemResult
{
    int                      result;
    std::vector<std::string> output;
};

time_t       fileModifiedTimestamp (const std::string& filename);
time_t       currentSystemTime();
std::string  percentageLabel (float pct);
SystemResult getOutputOfSystemCommand (std::string cmd, bool captureStderr);

#endif      //  SYSTEM_META_H_INCLUDED
