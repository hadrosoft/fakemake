#include <iostream>

#include <core/system.h>
#include <core/file_system.h>
#include <core/file_directory.h>
#include <core/string.h>
#include <core/iofile.h>
#include <core/common.h>

#include "system_meta.h"
#include "settings.h"
#include "build_target.h"
#include "terminal_color.h"

void init();
int  exitFakemake (int code);
bool changeDefaultTarget();
bool getCommandLineArgs (int argc, char* argv[]);
void printSplash (bool overrideQuiet = false);
void printHelp();
void printDryRun();
bool findMakefile();
bool parseMakefile();
bool getCompilerAndLinker();
bool collectBuildInfo();
void fixAllPaths();

/*
========================================
*/

void init()
{
    startTime = currentSystemTime();
    std::cout << std::boolalpha;
    get_exepath();

    char buffer[FILENAME_MAX];
    cwd = getcwd(buffer, FILENAME_MAX);
    if (!cwd.empty() && cwd.back() != '/') { cwd.push_back('/'); }
}

/*
========================================
*/

int exitFakemake (const int code)
{
    if (code == 0) {
        if (!doNothing) {
            std::cout << std::endl << "Process succeeded (";
            const clock_t elapsed = currentSystemTime() - startTime;
            const int     seconds = std::max((int)elapsed, 1);
            const int     minutes = seconds / 60;
            const int     hours   = minutes / 60;
            if (hours > 0) {
                const std::string hourPlural = (hours == 1) ? "" : "s";
                std::cout << hours << " hour" << hourPlural << ", ";
            }
            const std::string minutePlural = (minutes == 1) ? "" : "s";
            const std::string secondPlural = (seconds == 1) ? "" : "s";
            std::cout << minutes << " minute" << minutePlural << ", " << seconds << " second" << secondPlural << ")" << std::endl;
        }
    }
    else {
        std::cout << std::endl << "Aborting with exit code " << code << std::endl;
    }
    return code;
}

/*
========================================
*/

bool changeDefaultTarget()
{
    if (newTarget.empty()) { return true; }

    std::vector<std::string> lines;

    {
        const iofile file(makefilepath, File_Access::Input, File_Data::Ascii);
        while (!file.eof())
        {
            std::string line = file.line_input();
            if (lcase(string_word(line, 1)) == "default_target") {
                if (count_words_in_string(line) < 2) {
                    return false;
                }
                else {
                    string_replace(line, string_word(line, 2), newTarget);
                }
            }
            lines.push_back(line);
        }
    }

    const std::string backupFilename = makefilepath + "_bak.txt";
    copy_file(makefilepath, backupFilename, true);

    {
        const iofile file(makefilepath, File_Access::Output, File_Data::Ascii);
        for (const std::string& line: lines) {
            file.write_string(line);
        }
    }

    kill_file(backupFilename);
    defaultTarget = newTarget;
    return true;
}

/*
========================================
*/

bool getCommandLineArgs (int argc, char* argv[])
{
    const std::string flagDryRun = "--dry-run";

    for (int index = 1; index < argc; ++index) {
        const std::string arg(argv[index]);
        if (!arg.empty() && arg.front() == '-') {
            if (arg == "-h" || arg == "--help") {
                help    = true;
                version = false;
                return true;
            }
            else if (arg == "-v" || arg == "--version") {
                version = true;
                help    = false;
                return true;
            }
            else if (arg == "--clean") {
                clean = true;
            }
            else if (arg == "-q" || arg == "--quiet") {
                quiet = true;
            }
            #ifdef __linux__
            else if (arg == "--no-color") {
                color = false;
            }
            #endif
            else if (safe_substr(arg, 0, flagDryRun.size()) == flagDryRun) {
                dryRun = true;
                if (safe_substr(arg, flagDryRun.size(), 3) == "=la") {
                    dryRunAll = true;
                }
            }
            else if (arg == "--fakefile" || arg == "--file") {
                if (index < argc - 1) {
                    if (std::string(argv[index + 1]).front() == '"') {
                        makefilename.clear();
                        while (true)
                        {
                            makefilename += argv[index++];
                            if (index >= argc || makefilename.back() == '"') { break; }
                        }
                        makefilename = trim(makefilename, "\"");
                    }
                    else {
                        makefilename = argv[++index];
                    }
                }
                else {
                    printSplash(true);
                    std::cout << arg << ": no file specified" << std::endl;
                    return false;
                }
            }
            else if (arg == "--new-default-target") {
                if (index < argc - 1) {
                    newTarget = argv[++index];
                }
                else {
                    printSplash(true);
                    std::cout << arg << ": no target specified" << std::endl;
                    return false;
                }
            }
            else {
                printSplash(true);
                std::cout << "Error: Unknown switch: " << arg << std::endl;
                return false;
            }
        }
        //  Anything else, put into potential build targets list
        else {
            if (std::string(argv[index]) == "all") { buildAllTargets = true; }
            specifiedBuildNames.push_back(argv[index]);
        }
    }

    if (buildAllTargets) { specifiedBuildNames.clear(); }
    return true;
}

/*
========================================
*/

void printSplash (const bool overrideQuiet /*= false*/)
{
    if (version) {
        std::cout << "FakeMake v" << VERSION_NUMBER << std::endl;
    }
    else {
        if (quiet && !overrideQuiet) { return; }
        std::cout << "FakeMake build utility" << std::endl;
    }
    std::cout << "Copyright (c) 2021 Clarissa Toney. All rights reserved." << std::endl;
    std::cout << "Source code is available under the MIT License." << std::endl;
    std::cout << "https://gitlab.com/hadrosoft/fakemake" << std::endl << std::endl;
}

/*
========================================
*/

void printHelp()
{
    std::cout << "Usage: fakemake [options] TargetName" << std::endl;
    std::cout << "Options:" << std::endl;
    std::cout << "--help | -h          Display this help message" << std::endl;
    std::cout << "--quiet | -q         Don't print splash header" << std::endl;
    #ifdef __linux__
    std::cout << "--no-color           Don't print messages in color" << std::endl;
    #endif
    std::cout << "--version | -v       Display version information (overrides --quiet)" << std::endl;
    std::cout << "--clean              Clean the target (delete all object files) and re-build" << std::endl;
    std::cout << "--dry-run[=la]       Print a log of make operations, but don't modify any files" << std::endl;
    std::cout << "                     =la: Print final list of all source files" << std::endl;
    std::cout << "--fakefile           Specify the definition file to use. If this switch is not" << std::endl;
    std::cout << "                     used, then the default (\"Fakefile\" in the current working" << std::endl;
    std::cout << "                     directory) is used." << std::endl;
    std::cout << "--file               Same as --fakefile." << std::endl;
    std::cout << "--new-default-target Set a new default target (the target that is built if no" << std::endl;
    std::cout << "                     target is specified). This will write the new default" << std::endl;
    std::cout << "                     to Fakefile." << std::endl;
    std::cout << std::endl;
    std::cout <<
        "Any tokens not recognized as a switch will be assumed to be a desired build\n"
        "target. FakeMake can build multiple targets; just list them in a row, e.g.:\n\n"
        "    fakemake --clean Release Development Debug\n\n"
        "You can also build all targets specified in Fakefile by using the symbolic\n"
        "target \"all\"."
        <<
    std::endl;
}

/*
========================================
*/

void printDryRun()
{
    std::cout <<
        "(Dry run; no files will be modified or removed)" <<
        std::endl << std::endl;

    for (const BuildTarget& target: activeBuildTargets) {
        std::cout << FG_LIGHT_CYAN << "----------- Dry run: " << target.name << " -----------" << FG_DEFAULT << std::endl;
        std::cout << FG_LIGHT_YELLOW << "Compiler:              " << FG_DEFAULT << compiler << std::endl;
        std::cout << FG_LIGHT_YELLOW << "Linker:                " << FG_DEFAULT << linker << std::endl;
        std::cout << FG_LIGHT_YELLOW << "EXE name:              " << FG_DEFAULT << target.exeFilename << std::endl;
        if (!preBuildCommands.empty()) {
            std::cout << FG_LIGHT_YELLOW << "Pre-build commands:    " << FG_DEFAULT << std::endl;
            for (const std::string& cmd: preBuildCommands) {
                std::cout << "    " << cmd << std::endl;
            }
        }
        if (!postBuildCommands.empty()) {
            std::cout << FG_LIGHT_YELLOW << "Post-build commands:   " << FG_DEFAULT << std::endl;
            for (const std::string& cmd: postBuildCommands) {
                std::cout << "    " << cmd << std::endl;
            }
        }

        std::cout << FG_LIGHT_YELLOW << "Compiler switches:    " << FG_DEFAULT;
        if (target.compilerSwitches.empty() && globalCompilerSwitches.empty()) {
            std::cout << " (none)" << std::endl;
        }
        else {
            for (const std::string& str: target.compilerSwitches) { std::cout << " " << str; }
            const bool parens =
            #ifdef __linux__
            !color;
            #else
            true;
            #endif

            if (parens) {
                std::cout << " (";
            }
            else {
                std::cout << FG_LIGHT_CYAN << " ";
            }

            for (const std::string& str: globalCompilerSwitches) {
                if (&str == &globalCompilerSwitches.front()) {
                    std::cout << str;
                }
                else if (parens &&
                         &str == &globalCompilerSwitches.back())
                {
                    std::cout << " " << str << ")";
                }
                else {
                    std::cout << " " << str;
                }
            }
        }
        std::cout << std::endl;

        if (!target.fileSwitches.empty()) {
            std::cout << FG_LIGHT_YELLOW << "Per-file switches:    " << FG_DEFAULT;
            std::cout << std::endl;
            for (const FileSwitch& opt: target.fileSwitches) {
                std::cout << "    " << opt.filename << ":";
                for (const std::string& str: opt.switches) {
                    std::cout << " " << str;
                }
                std::cout << std::endl;
            }
            std::cout << std::endl;
        }
        else {
            std::cout << FG_LIGHT_YELLOW << "Per-file switches:     " << FG_DEFAULT << "(none)" << std::endl;
        }

        std::cout << FG_LIGHT_YELLOW << "Defines:              " << FG_DEFAULT;
        if (target.defines.empty() && globalDefines.empty()) {
            std::cout << " (none)" << std::endl;
        }
        else {
            for (const std::string& str: target.defines) { std::cout << " " << str; }
            const bool parens =
            #ifdef __linux__
            !color;
            #else
            true;
            #endif

            if (parens) {
                std::cout << " (";
            }
            else {
                std::cout << FG_LIGHT_CYAN << " ";
            }

            for (const std::string& str: globalDefines) {
                if (&str == &globalDefines.front()) {
                    std::cout << str;
                }
                else if (parens &&
                         &str == &globalDefines.back())
                {
                    std::cout << " " << str << ")";
                }
                else {
                    std::cout << " " << str;
                }
            }
        }
        std::cout << std::endl;

        std::cout << FG_LIGHT_YELLOW << "Linker switches:      " << FG_DEFAULT;
        if (target.compilerSwitches.empty() && globalCompilerSwitches.empty()) {
            std::cout << " (none)" << std::endl;
        }
        else {
            for (const std::string& str: target.linkerSwitches) { std::cout << " " << str; }
            const bool parens =
            #ifdef __linux__
            !color;
            #else
            true;
            #endif

            if (parens) {
                std::cout << " (";
            }
            else {
                std::cout << FG_LIGHT_CYAN << " ";
            }

            for (const std::string& str: globalLinkerSwitches) {
                if (&str == &globalLinkerSwitches.front()) {
                    std::cout << str;
                }
                else if (parens &&
                         &str == &globalLinkerSwitches.back())
                {
                    std::cout << " " << str << ")";
                }
                else {
                    std::cout << " " << str;
                }
            }
        }
        std::cout << std::endl;

        std::cout << FG_LIGHT_YELLOW << "Source extensions:    " << FG_DEFAULT;
        if (target.sourceTypes.empty()) {
            std::cout << " (none)" << std::endl;
        }
        else {
            for (const std::string& str: target.sourceTypes) { std::cout << " " << str; }
        }
        std::cout << std::endl;

        std::cout << FG_LIGHT_YELLOW << "Object file directory: " << FG_DEFAULT << target.objDirectory << std::endl;

        std::cout << std::endl;

        std::cout << FG_LIGHT_YELLOW << "Source directories:" << FG_DEFAULT << std::endl;
        if (target.sourceDirectories.empty()) {
            std::cout << "    (none)" << std::endl;
        }
        else {
            for (const SourceDir& entry: target.sourceDirectories) {
                std::cout << "    " << entry.directory;
                if (entry.recursive) { std::cout << " (recursive)"; }
                std::cout << std::endl;
            }
        }

        std::cout << FG_LIGHT_YELLOW << "Extra source files:        " << FG_DEFAULT << std::endl;
        if (target.extraSourceFiles.empty()) {
            std::cout << "    (none)" << std::endl;
        }
        else {
            for (const std::string& str: target.extraSourceFiles) {
                std::cout << "    " << str << std::endl;
            }
        }

        std::cout << FG_LIGHT_YELLOW << "Exclude source files:      " << FG_DEFAULT << std::endl;
        if (target.excludeSourceFiles.empty()) {
            std::cout << "    (none)" << std::endl;
        }
        else {
            for (const std::string& str: target.excludeSourceFiles) {
                std::cout << "    " << str << std::endl;
            }
        }

        std::cout << FG_LIGHT_YELLOW << "Include directories:       " << FG_DEFAULT << std::endl;
        if (target.includeDirectories.empty()) {
            std::cout << "    (none)" << std::endl;
        }
        else {
            for (const std::string& str: target.includeDirectories) {
                std::cout << "    " << str << std::endl;
            }
        }

        std::cout << FG_LIGHT_YELLOW << "Linker library directories:" << FG_DEFAULT << std::endl;
        if (target.linkerLibraryDirectories.empty()) {
            std::cout << "    (none)" << std::endl;
        }
        else {
            for (const std::string& str: target.linkerLibraryDirectories) {
                std::cout << "    " << str << std::endl;
            }
        }

        std::cout << FG_LIGHT_YELLOW << "Linker libraries:" << FG_DEFAULT << std::endl;
        if (target.linkerLibraries.empty()) {
            std::cout << "    (none)" << std::endl;
        }
        else {
            for (const std::string& str: target.linkerLibraries) {
                std::cout << "    " << str << std::endl;
            }
        }

        if (dryRunAll) {
            std::cout << FG_LIGHT_YELLOW << "All source files:" << FG_DEFAULT << std::endl;
            if (target.allSourceFiles.empty()) {
                std::cout << "    (none)" << std::endl;
            }
            else {
                for (const SourceObjPair& entry: target.allSourceFiles) {
                    std::cout << "    " << entry.sourceFile << std::endl;
                }
            }

            std::cout << FG_LIGHT_YELLOW << "All object files:" << FG_DEFAULT << std::endl;
            if (target.allSourceFiles.empty()) {
                std::cout << "    (none)" << std::endl;
            }
            else {
                for (const SourceObjPair& entry: target.allSourceFiles) {
                    std::cout << "    " << entry.objectFile << std::endl;
                }
            }
        }

        if (&target != &activeBuildTargets.back()) {
            std::cout << std::endl;
        }
    }
}

/*
========================================
*/

bool findMakefile()
{
    if (file_exists(canonicalized_path(makefilename))) {
        makefilepath = canonicalized_path(makefilename);
    }
    else {
        const directory_listing findfake(cwd, "*", false);
        for (const file_info& entry: findfake) {
            if (lcase(entry.filename()) == lcase(makefilename)) {
                makefilepath = findfake.get_full_filename(entry);
                break;
            }
        }
    }

    if (!file_exists(makefilepath)) {
        std::cout << "Error: Fakefile '" << makefilepath << "' doesn't exist" << std::endl;
        return false;
    }
    else if (file_size(makefilepath) == 0) {
        std::cout << "Error: cannot parse an empty Fakefile" << std::endl;
        return false;
    }

    return true;
}

/*
========================================
*/

bool parseMakefile()
{
    if (!file_exists(makefilepath)) {
        std::cout << "Error: " << makefilepath << " doesn't exist" << std::endl;
        return false;
    }

    if (!getCompilerAndLinker()) { return false; }
    if (!collectBuildInfo()) { return false; }
    for (BuildTarget& target: activeBuildTargets) { target.parse(); }

    return true;
}

/*
========================================
*/

bool getCompilerAndLinker()
{
    bool inABuild = false;
    const iofile file(makefilepath, File_Access::Input, File_Data::Ascii);
    while (!file.eof())
    {
        const std::string line   = file.line_input();
        const std::string first  = string_word(line, 1);
        const std::string second = string_word(line, 2);
        if (lcase(first) == "build") {
            if (inABuild) {
                std::cout << "Error: improperly nested build definitions; did you forget an 'end_build'?" << std::endl;
                return false;
            }
            inABuild = true;
        }
        else if (lcase(first) == "end_build") {
            if (!inABuild) {
                std::cout << "Error: improperly nested build definitions; did you forget a 'build [Name]'?" << std::endl;
                return false;
            }
            inABuild = false;
        }
        else if (lcase(first) == "defines") {
            if (!inABuild) {
                while (!file.eof())
                {
                    const std::string defineLine  = file.line_input();
                    const std::string defineFirst = string_word(defineLine, 1);
                    if (lcase(defineFirst) == "end") {
                        break;
                    }
                    else {
                        for (int wordId = 1;; ++wordId) {
                            const std::string token = string_word(defineLine, wordId);
                            //  We could check for other characters, but
                            //  FakeMake's comment marker is #. If we read
                            //  anything else and it turns out to be invalid,
                            //  Open Watcom will tell the user.
                            if (token.empty() || token.front() == '#') { break; }
                            globalDefines.push_back(token);
                        }
                    }
                }
            }
        }
        else if (lcase(first) == "compiler_switches") {
            if (!inABuild) {
                while (!file.eof())
                {
                    const std::string switchLine  = file.line_input();
                    const std::string switchFirst = string_word(switchLine, 1);
                    if (lcase(switchFirst) == "end") {
                        break;
                    }
                    else {
                        for (int wordId = 1;; ++wordId) {
                            const std::string token = string_word(switchLine, wordId);
                            //  We could check for other characters, but
                            //  FakeMake's comment marker is #. If we read
                            //  anything else and it turns out to be invalid,
                            //  Open Watcom will tell the user.
                            if (token.empty() || token.front() == '#') { break; }
                            globalCompilerSwitches.push_back(token);
                        }
                    }
                }
            }
        }
        else if (lcase(first) == "linker_switches") {
            if (!inABuild) {
                while (!file.eof())
                {
                    const std::string switchLine  = file.line_input();
                    const std::string switchFirst = string_word(switchLine, 1);
                    if (lcase(switchFirst) == "end") {
                        break;
                    }
                    else {
                        for (int wordId = 1;; ++wordId) {
                            const std::string token = string_word(switchLine, wordId);
                            //  We could check for other characters, but
                            //  FakeMake's comment marker is #. If we read
                            //  anything else and it turns out to be invalid,
                            //  Open Watcom will tell the user.
                            if (token.empty() || token.front() == '#') { break; }
                            globalLinkerSwitches.push_back(token);
                        }
                    }
                }
            }
        }
        else if (lcase(first) == "compiler") {
            compiler = second;
        }
        else if (lcase(first) == "linker") {
            linker = second;
        }
        else if (lcase(first) == "default_target") {
            defaultTarget = second;
        }
        else if (lcase(first) == "pre_build_command" &&
                 count_words_in_string(line) > 1)
        {
            preBuildCommands.push_back(safe_substr(line, string_word_char_pos(line, 2)));
        }
        else if (lcase(first) == "post_build_command" &&
                 count_words_in_string(line) > 1)
        {
            postBuildCommands.push_back(safe_substr(line, string_word_char_pos(line, 2)));
        }
    }

    if (specifiedBuildNames.empty() && !buildAllTargets) {
        std::cout << FG_YELLOW << "No build target specified; defaulting to \"" << defaultTarget << "\"" << FG_DEFAULT << std::endl;
        specifiedBuildNames.push_back(defaultTarget);
        std::cout << std::endl;
    }

    if (!compiler.empty() && !linker.empty()) { return true; }

    bool ret = true;

    if (compiler.empty()) {
        std::cout << "Error: compiler not specified" << std::endl;
        ret = false;
    }

    if (linker.empty()) {
        std::cout << "Error: linker not specified" << std::endl;
        ret = false;
    }

    return ret;
}

/*
========================================
*/

bool collectBuildInfo()
{
    std::vector<std::string> foundTargets;

    bool found = false;

    const iofile file(makefilepath, File_Access::Input, File_Data::Ascii);
    while (!file.eof())
    {
        const std::string line   = file.line_input();
        const std::string first  = string_word(line, 1);
        const std::string second = string_word(line, 2);

        if (lcase(first) == "build" && !second.empty()) {
            if (!vector_contains_value(foundTargets, second)) { foundTargets.push_back(second); }
            const bool matchFromCommandLineFound =
                (buildAllTargets || vector_contains_value(specifiedBuildNames, second));
            if (matchFromCommandLineFound) {
                BuildTarget* target = getBuildTarget(second);
                //  If it already exists (i.e., was specified twice in the
                //  Fakefile), don't throw a warning -- just clear it and start
                //  over
                if (target != nullptr) {
                    (*target) = BuildTarget();
                }
                //  Otherwise, create a new one
                else {
                    activeBuildTargets.emplace_back();
                    target = &activeBuildTargets.back();
                    target->name = second;
                }

                if (buildAllTargets &&
                    !vector_contains_value(specifiedBuildNames, second))
                {
                    specifiedBuildNames.push_back(second);
                }

                //  Parse the file, reading every line in the target definition
                while (!file.eof())
                {
                    const std::string newLine  = file.line_input();
                    const std::string newFirst = string_word(line, 1);
                    if (lcase(newLine) == "end_build") {
                        break;
                    }
                    else {
                        const std::string infoLine = trim(newLine);
                        if (!infoLine.empty() && infoLine.front() != '#') {
                            target->buildLines.push_back(infoLine);
                        }
                    }
                }
                if (target->buildLines.empty()) {
                    std::cout << "Error: build target \"" << target->name << "\" contains no information" << std::endl;
                    return false;
                }
                else {
                    found = true;
                }
            }
            //  If this build was not specified on the command line, just skip
            //  the entire block and move on (ignore it)
            else {
                while (!file.eof())
                {
                    const std::string newLine  = file.line_input();
                    const std::string newFirst = string_word(newLine, 1);
                    if (lcase(newFirst) == "end_build") { break; }
                }
            }
        }
    }

    if (!buildAllTargets) {
        for (const std::string& requested: specifiedBuildNames) {
            if (!vector_contains_value(foundTargets, requested)) {
                std::cout << FG_RED << "Error: requested target \"" << requested << "\" does not exist in definition file." << FG_DEFAULT << std::endl;
                found = false;
            }
        }
    }

    return found;
}

/*
========================================
*/

void fixAllPaths()
{
    for (BuildTarget& target: activeBuildTargets) { target.fixPaths(); }
}

/*
========================================
*/

int main (int argc, char* argv[])
{
    init();
    if (!getCommandLineArgs(argc, argv)) { return exitFakemake(1); }
    printSplash();

    if (version) { return 0; }

    if (help) {
        printHelp();
        return 0;
    }

    if (!findMakefile()) { return exitFakemake(2); }
    if (!changeDefaultTarget()) { return exitFakemake(7); }
    if (!parseMakefile()) { return exitFakemake(3); }
    fixAllPaths();

    for (BuildTarget& target: activeBuildTargets) {
        target.compileFinalSourceFileList();
        target.generateObjectFilePaths();
    }

    if (dryRun) {
        printDryRun();
    }
    else {
        for (const std::string& cmd: preBuildCommands) {
            if (std::system(cmd.c_str()) != 0) {
                std::cout << "Pre-build command \"" << cmd << "\" failed" << std::endl;
                return exitFakemake(6);
            }
        }
        if (!preBuildCommands.empty()) { std::cout << std::endl; }

        for (BuildTarget& target: activeBuildTargets) {
            if (!target.compile(clean)) { return exitFakemake(4); }
            if (!target.link()) { return exitFakemake(5); }
            if (activeBuildTargets.size() > 1 && &target != &activeBuildTargets.back()) { std::cout << std::endl; }
        }
        for (const std::string& cmd: postBuildCommands) {
            if (std::system(cmd.c_str()) != 0) {
                std::cout << "Post-build command \"" << cmd << "\" failed" << std::endl;
                return exitFakemake(6);
            }
        }
        if (!postBuildCommands.empty()) { std::cout << std::endl; }
    }

    return exitFakemake(0);
}
