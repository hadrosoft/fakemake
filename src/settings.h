#ifndef SETTINGS_H_INCLUDED
#define SETTINGS_H_INCLUDED

#include <string>
#include <vector>

extern const std::string VERSION_NUMBER;

#ifdef __linux__
extern bool color;
#endif
extern bool                     version;
extern std::string              newTarget;
extern bool                     help;
extern bool                     clean;
extern bool                     quiet;
extern bool                     dryRun;
extern bool                     dryRunAll;
extern bool                     doNothing;
extern bool                     buildAllTargets;

extern std::string              cwd;
extern std::string              makefilename;
extern std::string              makefilepath;
extern std::string              defaultTarget;
extern std::vector<std::string> preBuildCommands;
extern std::vector<std::string> postBuildCommands;
extern std::vector<std::string> globalDefines;
extern std::vector<std::string> globalCompilerSwitches;
extern std::vector<std::string> globalLinkerSwitches;
extern time_t                   startTime;
extern std::string              compiler;
extern std::string              linker;
extern std::vector<std::string> specifiedBuildNames;   //  Target names from command line

#endif      //  SETTINGS_H_INCLUDED
