#include "system_meta.h"

#include <iostream>

#include <sys/stat.h>
#include <limits.h>

#include <core/string.h>
#include <core/math.h>

/*
========================================
*/

time_t fileModifiedTimestamp (const std::string& filename)
{
    struct stat attr;
    stat(filename.c_str(), &attr);
    return attr.st_mtime;
}

/*
========================================
*/

time_t currentSystemTime()
{
    return time(nullptr);
}

/*
========================================
*/

std::string percentageLabel (const float pct)
{
    const std::string pad   = (pct < 10) ? "  " : (pct < 100) ? " " : "";
    std::string       token = pad + numstr(pct, 1);
    if (frac(strval(token)) == 0.0f) { token += ".0"; }
    return token;
}

/*
========================================
*/

SystemResult getOutputOfSystemCommand (
    std::string cmd, const bool captureStderr)
{
    if (captureStderr) { cmd = "(" + cmd + ") 2>&1"; }

    FILE* const file = popen(cmd.c_str(), "r");
    if (file == nullptr) { return {-1, {}}; }

    SystemResult ret;

    char buf[PATH_MAX];
    while (fgets(buf, PATH_MAX, file) != nullptr)
    {
        ret.output.push_back(std::string(buf));
    }

    const int status = pclose(file);
    if (status == -1) {
        std::cout << "Error: Could not close command stream" << std::endl;
        exit(-1);
    }

    ret.result = WEXITSTATUS(status);
    return ret;
}
