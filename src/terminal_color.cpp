#include "terminal_color.h"

#include "settings.h"

/*
========================================
*/

std::ostream& operator << (std::ostream& os, Terminal_Color_Code code)
{
    #ifdef __linux__
        if (color) {
            return os << "\033[" << static_cast<int>(code) << "m";
        }
        else {
            return os << "";
        }
    #else
        return os << "";
    #endif
}
