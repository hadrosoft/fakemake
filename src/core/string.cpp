#include "string.h"

#include <algorithm>
#include <codecvt>
#include <locale>

#include "math.h"
#include "exception.h"

/*
========================================
*/

bool is_newline (const char c)
{
    return (c == '\n' || c == '\r' || c == '\f');
}

/*
========================================
*/

bool is_space (const char c)
{
    return (c == ' ');
}

/*
========================================
*/

bool is_dash (const char c)
{
    return (c == '-' || c == static_cast<char>(150) || c == static_cast<char>(151));
}

/*
========================================
*/

bool is_number (const std::string& str)
{
    if (str.empty()) { return false; }
    char* end;
    std::strtod(str.c_str(), &end);
    return (*end == 0);
}

/*
========================================
*/

std::string string_word (const std::string&  str, const size_t word_number)
{
    std::stringstream ss(str);
    std::string       line;
    size_t            word_count = 0;

    while (std::getline(ss, line)) {
        size_t prev = 0;
        size_t pos;

        while ((pos = line.find_first_of(" \n\t\v\b\r\f\a\0", prev)) != std::string::npos) {
            if (pos > prev) {
                ++word_count;
                if (word_count == word_number) {
                    return trim(line.substr(prev, pos - prev));
                }
            }
            prev = pos + 1;
        }

        if (prev < line.length()) {
            ++word_count;
            if (word_count == word_number) {
                return trim(line.substr(prev, pos - prev));
            }
        }
    }
    return "";
}

/*
========================================
*/

size_t string_word_char_pos (
    const std::string& str, const size_t word_number)
{
    std::stringstream ss(str);
    std::string       line;
    size_t            word_count = 0;

    while (std::getline(ss, line)) {
        size_t prev = 0;
        size_t pos;

        while ((pos = line.find_first_of(" \n\t\v\b\r\f\a\0", prev)) != std::string::npos) {
            if (pos > prev) {
                ++word_count;
                if (word_count == word_number) { return prev; }
            }
            prev = pos + 1;
        }

        if (prev < line.length()) {
            ++word_count;
            if (word_count == word_number) { return prev; }
        }
    }
    return std::string::npos;
}

/*
========================================
*/

size_t count_words_in_string (const std::string& str)
{
    if (str == "") { return 0; }

    std::stringstream ss(str);
    std::string       line;
    size_t            word_count = 0;

    while (std::getline(ss, line)) {
        size_t prev = 0;
        size_t pos;

        while ((pos = line.find_first_of(" \n\t\v\b\r\f\a\0", prev)) != std::string::npos) {
            if (pos > prev) { ++word_count; }
            prev = pos + 1;
        }
        if (prev < line.length()) { ++word_count; }
    }
    return word_count;
}

/*
========================================
*/

size_t count_lines_in_string (const std::string& str)
{
    return (str.empty()) ? 0 : std::count(str.begin(), str.end(), '\n') + 1;
}

/*
========================================
*/

std::vector<std::string> all_lines_in_string (const std::string& str)
{
    std::vector<std::string> ret;
    if (str.empty()) { return ret; }
    std::stringstream ss(str);
    std::string       to;
    while (std::getline(ss, to, '\n')) { ret.push_back(to); }
    if (str.back() == '\n') { ret.push_back(""); }
    return ret;
}

/*
========================================
*/

std::vector<std::string> all_words_in_string (const std::string& str)
{
    std::vector<std::string> ret;

    for (size_t index = 1; ; ++index) {
        const std::string word = string_word(str, index);
        if (word.empty()) { break; }
        ret.push_back(word);
    }

    return ret;
}

/*
========================================
*/

std::string ucase (const std::string& str)
{
    std::string           new_string = str;
    std::string::iterator char_pos   = new_string.begin();
    std::string::iterator end        = new_string.end();

    while (char_pos != end) {
        *char_pos = std::toupper(static_cast<unsigned char>(*char_pos));
        ++char_pos;
    }
    return new_string;
}

/*
========================================
*/

std::string lcase (const std::string& str)
{
    std::string           new_string = str;
    std::string::iterator char_pos   = new_string.begin();
    std::string::iterator end        = new_string.end();

    while (char_pos != end) {
        *char_pos = std::tolower((unsigned char)*char_pos);
        ++char_pos;
    }
    return new_string;
}

/*
========================================
*/

std::string initial_capital (std::string str)
{
    if (!str.empty()) { str.front() = std::toupper(str.front()); }
    return str;
}

/*
========================================
*/

std::string ltrim (
    const std::string& str, const std::string& trim_chars /*= " \t"*/)
{
    const auto begin = str.find_first_not_of(trim_chars);
    return (begin == std::string::npos) ? "" : safe_substr(str, begin);
}

/*
========================================
*/

std::string rtrim (
    const std::string& str, const std::string& trim_chars /*= " \t"*/)
{
    const auto end = str.find_last_not_of(trim_chars);
    return (end == std::string::npos) ? "" : safe_substr(str, 0, end + 1);
}

/*
========================================
*/

std::string trim (
    const std::string& str, const std::string& trim_chars /*= " \t"*/)
{
    return ltrim(rtrim(str, trim_chars), trim_chars);
}

/*
========================================
*/

std::vector<std::string> wrap_string_to_char_width (
    const std::string& str, size_t width)
{
    if (width < 1) { width = 1; }

    //  Split string into words
    std::vector<std::string> word_list;
    if (str.size() <= width) {
        word_list.push_back(str);
        return word_list;
    }

    size_t start_pos = 0;

    for (size_t char_pos = 0; char_pos < str.size(); ++char_pos) {
        if (char_pos == str.size() - 1) {
            word_list.push_back(str.substr(start_pos));
        }
        if (str[char_pos] == ' ' || str[char_pos] == '\n') {
            word_list.push_back(str.substr(start_pos, char_pos - (start_pos - 1)));
            start_pos = char_pos + 1;
        }
    }

    //  Combine words into lines
    std::vector<std::string> line_list;

    size_t      line_width = 0;
    std::string temp_line;

    for (size_t word_pos = 0; word_pos < word_list.size(); ++word_pos) {
        const std::string& word = word_list[word_pos];

        if (word[0] == '\n') {
            temp_line += word;
            line_list.push_back(temp_line);
            line_width = 0;
            temp_line.erase();
        }
        else if (word[0] == ' ') {
            temp_line += word;
            line_width += 1;
        }
        else {
            size_t word_width = word.size();
            if (line_width + word_width < width) {
                line_width += word_width;
                temp_line += word;
                if (word[word.size() - 1] == '\n') {
                    line_list.push_back(temp_line);
                    line_width = 0;
                    temp_line.erase();
                }
            }
            else {
                if (line_width == 0) {
                    /*  Go ahead and push the line; we've obviously reached a
                        word that's wider than our entire text box, and there's
                        nothing we can do about it. */
                    temp_line += word;
                    line_list.push_back(temp_line);
                    line_width = 0;
                    temp_line.erase();
                }
                else {
                    line_list.push_back(temp_line);
                    line_width = 0;
                    temp_line.erase();
                    --word_pos;
                }
            }
        }
    }

    line_list.push_back(temp_line);
    return line_list;
}

/*
========================================
*/

std::string safe_substr (
    const std::string&  str,
    const size_t        pos /*= 0*/,
    const size_t        n   /*= std::string::npos*/)
{
    if (str.empty() || pos >= str.size()) { return ""; }
    return str.substr(pos, std::min(n, str.size() - pos));
}

/*
========================================
*/

std::string safe_substr_range (
    const std::string& str, size_t first_char_pos, size_t last_char_pos)
{
    if (str.empty()) { return str; }
    clamp(first_char_pos, 0, str.size() - 1);
    clamp(last_char_pos,  0, str.size() - 1);
    if (first_char_pos > last_char_pos) {
        std::swap(first_char_pos, last_char_pos);
    }
    return safe_substr(str, first_char_pos, last_char_pos - first_char_pos + 1);
}

/*
========================================
*/

std::string safe_substr_to_char (
    const std::string& str, const size_t first_char_pos, const char find_char)
{
    if (str.empty() || first_char_pos >= str.size()) { return ""; }
    for (size_t index = first_char_pos; index < str.size(); ++index) {
        if (str[index] == find_char) {
            return safe_substr_range(str, first_char_pos, index);
        }
    }
    return "";
}

/*
========================================
*/

std::string safe_substr_to_chars (
    const std::string&  str,
    const size_t        first_char_pos,
    const std::string&  find_any_char)
{
    if (str.empty() || first_char_pos >= str.size()) { return ""; }
    for (size_t index = first_char_pos; index < str.size(); ++index) {
        if (find_any_char.find(str[index]) != std::string::npos) {
            return safe_substr_range(str, first_char_pos, index);
        }
    }
    return "";
}

/*
========================================
*/

bool string_contains_char (const std::string& str, const char char_to_find)
{
    return (str.find(char_to_find) != std::string::npos);
}

/*
========================================
*/

bool string_contains_any_char (const std::string& str, const std::string& chars)
{
    return (str.find_first_of(chars) != std::string::npos);
}

/*
========================================
*/

bool string_contains_substring (
    const std::string&  str,
    const std::string&  substring,
    const bool          case_sensitive  /*= false*/)
{
    if (!case_sensitive) {
        return (lcase(str).find(lcase(substring)) != std::string::npos);
    }
    return (str.find(substring) != std::string::npos);
}

/*
========================================
*/

bool string_match (
    const std::string&  str,
    const size_t        search_pos,
    const std::string&  search_str,
    const bool          case_sensitive  /*= true*/)
{
    if (!case_sensitive) {
        const std::string lhs = lcase(str);
        const std::string rhs = lcase(search_str);
        return (safe_substr(lhs, search_pos, rhs.size()) == rhs);
    }
    else {
        return (safe_substr(str, search_pos, search_str.size()) == search_str);
    }
}

/*
========================================
*/

void string_replace (
    std::string&        str,
    const std::string&  check_str,
    const std::string&  replace_str)
{
    size_t n = 0;
    while ((n = str.find(check_str, n)) != std::string::npos) {
        str.replace(n, check_str.size(), replace_str);
        n += replace_str.size();
    }
}


std::string string_replace (
    const std::string&  str,
    const std::string&  check_str,
    const std::string&  replace_str)
{
    std::string ret = str;
    string_replace(ret, check_str, replace_str);
    return ret;
}


void string_replace (
    std::string&        str,
    size_t              start_pos,
    size_t              end_pos,
    const std::string&  replace_str)
{
    if (start_pos > end_pos) { std::swap(start_pos, end_pos); }
    str.replace(start_pos, (end_pos - start_pos + 1), replace_str);
}


std::string string_replace (
    const std::string&  str,
    const size_t        start_pos,
    const size_t        end_pos,
    const std::string&  replace_str)
{
    std::string ret = str;
    string_replace(ret, start_pos, end_pos, replace_str);
    return ret;
}

/*
========================================
*/

void strip_chars (std::string& str, const std::string& chars_to_remove)
{
    for (char c: chars_to_remove) {
        str.erase(std::remove(str.begin(), str.end(), c), str.end());
    }
}


std::string strip_chars (const std::string& str, const std::string& chars_to_remove)
{
    std::string ret = str;
    strip_chars(ret, chars_to_remove);
    return ret;
}

/*
========================================
*/

void strip_repeated_chars (std::string& str, const std::string& chars_to_remove)
{
    for (size_t index = 0; index < str.size(); ++index) {
        const char c = str[index];
        if (chars_to_remove.find_first_of(c) != std::string::npos &&
            index < str.size() - 1 &&
            str[index + 1] == c)
        {
            str.erase(index, 1);
            --index;
        }
    }
}


std::string strip_repeated_chars (
    const std::string& str, const std::string& chars_to_remove)
{
    std::string ret = str;
    strip_repeated_chars(str, chars_to_remove);
    return ret;
}


/*
========================================
*/

void strip_repeated_whitespace (std::string& str)
{
    strip_repeated_chars(str, " \t");
}


std::string strip_repeated_whitespace (const std::string& str)
{
    std::string ret = trim(str);
    strip_repeated_whitespace(ret);
    return ret;
}

/*
========================================
*/

double strval (const std::string& str)
{
    return std::atof(str.c_str());
//    try {
//        return std::stod(str);
//    }
//    catch (const std::invalid_argument&) {
//        return 0;
//    }
}

/*
========================================
*/

bool bool_from_token (std::string token)
{
    token = lcase(token);
    return (token == "true"  || token == "yes") ? true  :
           (token == "false" || token == "no")  ? false : true;
}

/*
========================================
*/

std::string space_pad_back (const std::string& str, const size_t char_count)
{
    if (char_count > str.size()) {
        return str + std::string(char_count - str.size(), ' ');
    }
    else {
        return str;
    }
}

/*
========================================
*/

std::vector<std::string> split_string (
    const std::string& str, const char delimiter)
{
    std::vector<std::string> ret;
    size_t j = 0;
    for (size_t index = 0; index < str.size(); ++index) {
        if (str[index] == delimiter &&
            !(index > 0 && str[index - 1] == '\\'))
        {
            const std::string cur = str.substr(j, index - j);
            if (!cur.empty()) { ret.push_back(cur); }
            j = index + 1;
        }
    }
    if (j < str.size()) { ret.push_back(str.substr(j)); }
    const std::string del_str   (1, delimiter);
    const std::string escape  = "\\" + del_str;
    for (std::string& check: ret) { string_replace(check, escape, del_str); }
    return ret;
}

/*
========================================
*/

std::string intstr (const int val)
{
    return numstr(val);
}

/*
========================================
*/

std::string numstr_separators (const float val)
{
    if (std::fabs(val - static_cast<int>(val)) < 0.000001f) {
        return intstr_separators(val);
    }

    return intstr_separators(val) + safe_substr(numstr(frac(std::fabs(val))), 2);
}

/*
========================================
*/

std::string intstr_separators (const int val)
{
    std::string ret = intstr(val);
    int insert_position = ret.size() - 3;
    while (insert_position > 0) {
        ret.insert(insert_position, ",");
        insert_position -= 3;
    }

    return ret;
}

/*
========================================
*/

std::pair<std::string, size_t> read_quoted_string (
    const std::string& str, const size_t start_at_word /*= 1*/)
{
    const size_t pos = string_word_char_pos(str, start_at_word);
    if (pos == std::numeric_limits<size_t>::max()) {
        throw core::exception(ERROR_FILE_LINE);
    }

    for (size_t index = pos + 1; ; ++index) {
        if (str.at(index) == '"') {
            const std::string found_str = safe_substr(str, pos + 1, index - pos - 1);
            return std::make_pair(
                found_str,
                start_at_word + std::max(size_t(1), count_words_in_string(found_str)));
        }
    }
    throw core::exception(ERROR_FILE_LINE);
}

/*
========================================
*/

std::string get_property_from_string (
    const std::string&  line,
    const std::string&  property_name,
    const size_t        number_of_tokens    /*= 1*/)
{
    if (number_of_tokens == 0) { return ""; }

    const std::vector<std::string> words = all_words_in_string(line);
    std::string                    ret;
    bool                           found = false;

    for (const std::string& check: words) {
        if (found) {
            if (ret.size() < number_of_tokens) {
                ret += check + " ";
            }
            else {
                break;
            }
        }
        else if (check == property_name) {
            found = true;
        }
    }

    return trim(ret);
}

/*
========================================
*/

std::string letter_sequence (int index)
{
    std::string ret;

    while (index > 0) {
        int rem = index % 26;

        if (rem == 0) {
            ret.push_back('Z');
            index = (index / 26) - 1;
        }
        else {
            ret.push_back('A' + (rem - 1));
            index /= 26;
        }
    }
    std::reverse(ret.begin(), ret.end());
    return ret;
}

/*
========================================
*/

stringvec text_columns (
    const std::vector<std::string>& string_list,
    const size_t                    max_table_width /*= 80*/,
    const size_t                    column_padding  /*= 1*/)
{
    if (string_list.size() <= 1) { return string_list; }

    const auto get_max_width = [](const stringvec& v){ size_t ret = 0; for (const auto& s: v) { ret = std::max(ret, s.size()); } return ret; };
    const size_t max_item_width = get_max_width(string_list);
    if (max_item_width >= max_table_width) { return string_list; }

    const size_t num_columns = (max_table_width + column_padding) / (max_item_width + column_padding);

    size_t current_column = 1;
    stringvec   ret;
    std::string current_line;
    for (const std::string& str: string_list) {
        const bool   last_column = (current_column == num_columns);
        const size_t pad         = (last_column) ? max_item_width : max_item_width + column_padding;
        current_line += space_pad_back(str, pad);
        ++current_column;
        if (last_column) {
            ret.push_back(current_line);
            current_line.clear();
            current_column = 1;
        }
    }

    if (!current_line.empty()) { ret.push_back(current_line); }
    return ret;
}
