#include "file_system.h"

#include <iostream>
#include <algorithm>

#ifdef __linux__
    #include <experimental/filesystem>
    #include <unistd.h>
    #include <sys/types.h>
    #include <pwd.h>
    #include <stdlib.h>
#endif

#include "system.h"
#include "exception.h"
#include "string.h"

/*
========================================
*/

bool file_exists (const std::string& filename)
{
    #ifdef TARGET_OS_MAC
        #error macOS unsupported yet!
    #elif defined __linux__
        return fs::exists(filename);
    #elif defined _WIN32 || defined _WIN64

        const DWORD file_attrib = GetFileAttributes(filename.c_str());

        if (file_attrib == INVALID_FILE_ATTRIBUTES) {
            return false;
        }
        else {
            /*  Still go ahead and return false if hidden, system file,
                directory, etc. */
            if ((file_attrib & FILE_ATTRIBUTE_SYSTEM)     ||
                (file_attrib & FILE_ATTRIBUTE_TEMPORARY)  ||
                (file_attrib & FILE_ATTRIBUTE_OFFLINE)    ||
                (file_attrib & FILE_ATTRIBUTE_HIDDEN)     ||
                (file_attrib & FILE_ATTRIBUTE_DIRECTORY)  ||
                (file_attrib & FILE_ATTRIBUTE_COMPRESSED) ||
                (file_attrib & FILE_ATTRIBUTE_DEVICE)     ||
                (file_attrib & FILE_ATTRIBUTE_ENCRYPTED))
            {
                return false;
            }
            else {
                return true;
            }
        }

        return false;

    #else
        #error Unknown platform! Currently only Win32/64 and Linux are supported.
    #endif
}

/*
========================================
*/

bool directory_exists (
    const std::string& directory_name, const bool make_on_fail /*= false*/)
{
    #ifdef TARGET_OS_MAC
        #error macOS unsupported yet!
    #elif defined __linux__
        if (fs::exists(directory_name)) {
            if (!fs::is_directory(directory_name)) {
                throw core::exception(ERROR_FILE_LINE);
            }
            return true;
        }
        else {
            return (make_on_fail && create_directory(directory_name));
        }
    #elif defined _WIN32 || defined _WIN64

        const DWORD file_attrib = GetFileAttributes(directory_name.c_str());

        //  If file check fails, assume it doesn't exist, and try to create directory (if requested)
        if (file_attrib == INVALID_FILE_ATTRIBUTES) {
            if (make_on_fail) {
                if (CreateDirectory(directory_name.c_str(), nullptr)) {
                    return true;
                }
                else {
                    switch (GetLastError()) {
                        case ERROR_ALREADY_EXISTS: return true;
                        case ERROR_PATH_NOT_FOUND: return false;
                    }
                }
            }
            else {
                return false;
            }
        }

        if (file_attrib & FILE_ATTRIBUTE_DIRECTORY) { return true; }

        return false;

    #else
        #error Unknown platform! Currently only Win32/64 and Linux are supported.
    #endif
}

/*
========================================
*/

std::string get_directory (const std::string& path_name)
{
    char last_char = path_name.back();

    //  If the path is a directory, we're done
    if (last_char == '\\' || last_char == '/') { return path_name; }

    #ifdef TARGET_OS_MAC
        #error macOS unsupported yet!
    #elif defined __linux__
        fs::path path(path_name);
        if (fs::is_directory(path) || !path.has_filename()) { return path_name; }
        return path.remove_filename().string();
    #elif defined _WIN32 || defined _WIN64

        const DWORD file_attrib = GetFileAttributes(path_name.c_str());

        if (file_attrib == INVALID_FILE_ATTRIBUTES ||
            !(file_attrib & FILE_ATTRIBUTE_DIRECTORY))
        {
            for (size_t index = path_name.size(); index --> 0;) {
                last_char = path_name[index];
                if (last_char == '\\' || last_char == '/') {
                    return path_name.substr(0, index + 1);
                }
            }
        }
        else {
            //  It's a directory; we're done
            return path_name;
        }

        return exepath;

    #else
        #error Unknown platform! Currently only Win32/64 and Linux are supported.
    #endif
}

/*
========================================
*/

bool create_directory (const std::string& directory_name)
{
    if (directory_exists(directory_name, false)) { return true; }

    #ifdef TARGET_OS_MAC
        #error macOS unsupported yet!
    #elif defined __linux__
        return fs::create_directories(directory_name);
    #elif defined _WIN32 || defined _WIN64
        return CreateDirectory(directory_name.c_str(), nullptr);
    #else
        #error Unknown platform! Currently only Win32/64 and Linux are supported.
    #endif
}

/*
========================================
*/

std::string extract_filename (const std::string& path_name)
{
    if (path_name.back() == '\\' || path_name.back() == '/') { return ""; }

    for (size_t index = path_name.size(); index --> 0;) {
        if (path_name[index] == '\\' || path_name[index] == '/') {
            return path_name.substr(index + 1);
        }
    }

    return path_name;
}

/*
========================================
*/

std::string strip_file_extension (const std::string& filename)
{
    if (filename.empty()) { return filename; }

    for (size_t index = filename.size(); index --> 0;) {
        if (filename[index] == '.' && index != 0) {
            return filename.substr(0, index);
        }
    }

    return filename;
}

/*
========================================
*/

bool kill_file (
    const std::string&  filename,
    const bool          backup_first    /*= false*/,
    std::string*        backup_filename /*= nullptr*/)
{
    if (backup_first) {
        if (!file_exists(filename + ".bak")) {
            const std::string target_name = filename + ".bak";
            const bool        success     = (std::rename(filename.c_str(), target_name.c_str()) == 0);
            if (success && backup_filename != nullptr) { *backup_filename = target_name; }
            return success;
        }
        else {
            size_t attempt = 0;
            while (true) {
                std::string target_name = filename + ".bak" + numstr(attempt);
                if (file_exists(target_name)) {
                    ++attempt;
                    continue;
                }
                else {
                    const bool success = (std::rename(filename.c_str(), target_name.c_str()) == 0);
                    if (success && backup_filename != nullptr) { *backup_filename = target_name; }
                    return success;
                }
            }
        }
        return false;
    }

    return fs::remove_all(filename.c_str());
}

/*
========================================
*/

bool copy_file (
    const std::string&  source_filename,
    const std::string&  new_filename,
    const bool          overwrite_if_exists)
{
    if (!file_exists(source_filename)) { return false; }
    if (file_exists(new_filename) && !overwrite_if_exists) { return false; }

    kill_file(new_filename);

    #ifdef TARGET_OS_MAC
        #error macOS unsupported yet!
    #elif defined __linux__
        fs::copy(source_filename, new_filename);
        return file_exists(new_filename);
    #elif defined _WIN32 || defined _WIN64
        return CopyFile(source_filename.c_str(), new_filename.c_str(), TRUE);
    #else
        #error Unknown platform! Currently only Win32/64 and Linux are supported.
    #endif
}

/*
========================================
*/

bool rename_file (
    const std::string& old_filename, const std::string& new_filename)
{
    return (std::rename(old_filename.c_str(), new_filename.c_str()) == 0);
}

/*
========================================
*/

size_t file_size (const std::string& filename)
{
    #ifdef TARGET_OS_MAC
        #error macOS unsupported yet!
    #elif defined __linux__
        if (fs::is_regular_file(filename)) {
            return fs::file_size(filename);
        }
        else {
            std::cout << "Reading file '" << filename << "'" << std::endl;
            throw core::exception(ERROR_FILE_LINE);
        }
    #elif defined _WIN32 || defined _WIN64

        WIN32_FIND_DATA file_data;
        const HANDLE    file_handle = FindFirstFile(filename.c_str(), &file_data);

        if (file_handle != INVALID_HANDLE_VALUE) {
            return file_data.nFileSizeLow;
            FindClose(file_handle);
        }
        else {
            throw core::exception(ERROR_FILE_LINE);
        }

    #else
        #error Unknown platform! Currently only Win32/64 and Linux are supported.
    #endif
}

/*
========================================
*/

File_Type file_type (const std::string& filename)
{
    #ifdef TARGET_OS_MAC
        #error macOS unsupported yet!
    #elif defined __linux__
        return  (!fs::exists(filename)) ? File_Type::Does_Not_Exist :
                (fs::is_directory(filename)) ? File_Type::Directory : File_Type::File;
    #elif defined _WIN32 || defined _WIN64

        const DWORD file_attrib = GetFileAttributes(filename.c_str());
        return  (file_attrib == INVALID_FILE_ATTRIBUTES) ? File_Type::Does_Not_Exist :
                (file_attrib & FILE_ATTRIBUTE_DIRECTORY) ? File_Type::Directory : File_Type::File;

    #else
        #error Unknown platform! Currently only Win32/64 and Linux are supported.
    #endif
}

/*
========================================
*/

std::string canonicalized_path (std::string path_name)
{
    #if defined _WIN32 || defined _WIN64
        TCHAR target_buffer[MAX_PATH];
        const DWORD strLen = GetFullPathName(path_name.c_str(), MAX_PATH, target_buffer, nullptr);
        return (strLen != 0) ? std::string(target_buffer) : path_name;
    #else
        std::replace(path_name.begin(), path_name.end(), '\\', '/');

        std::vector<std::string> ps = split_string(path_name, '/');
        std::string p = "";
        std::vector<std::string> st;
        for (size_t index = 0; index < ps.size(); ++index) {
            if (ps[index] =="..") {
                if (!st.empty()) { st.pop_back(); }
            }
            else if (ps[index] != ".") {
                st.push_back(ps[index]);
            }
        }
        for (size_t index = 0; index < st.size(); ++index) {
            p += "/" + st[index];
        }
        return (p.empty()) ? "/" : p;
    #endif
}

/*
========================================
*/

bool file_is_safe_for_writing (const std::string& filename)
{
    #ifdef TARGET_OS_MAC
        #error macOS unsupported yet!
    #elif defined __linux__
        const std::ofstream ofs(filename);
        return ofs.is_open();
    #elif defined _WIN32 || defined _WIN64

        const DWORD file_attrib = GetFileAttributes(filename.c_str());

        if (file_attrib == INVALID_FILE_ATTRIBUTES) {
            //  If file doesn't exist, obviously okay to write to!
            if (GetLastError() == ERROR_FILE_NOT_FOUND) { return true; }
        }
        else {
            /*  Still go ahead and return false if hidden, system file,
                directory, etc. */
            if ((file_attrib & FILE_ATTRIBUTE_READONLY)   ||
                (file_attrib & FILE_ATTRIBUTE_SYSTEM)     ||
                (file_attrib & FILE_ATTRIBUTE_TEMPORARY)  ||
                (file_attrib & FILE_ATTRIBUTE_OFFLINE)    ||
                (file_attrib & FILE_ATTRIBUTE_HIDDEN)     ||
                (file_attrib & FILE_ATTRIBUTE_DIRECTORY)  ||
                (file_attrib & FILE_ATTRIBUTE_COMPRESSED) ||
                (file_attrib & FILE_ATTRIBUTE_DEVICE)     ||
                (file_attrib & FILE_ATTRIBUTE_ENCRYPTED))
            {
                return false;
            }
            else {
                return true;
            }
        }

        return false;

    #else
        #error Unknown platform! Currently only Win32/64 and Linux are supported.
    #endif
}

/*
========================================
*/

bool filename_wildcard_match (std::string mask, std::string filename)
{
    //  Written by Jack Handy -- jakkhandy@hotmail.com
    //  http://www.codeproject.com/Articles/1088/Wildcard-string-compare-globbing

    //  Note: we don't use it here, but if you need case-insensitive matching,
    //  wrap any character comparisons in lcase() or ucase() calls.

    if (mask.empty() || filename.empty()) { throw core::exception(ERROR_FILE_LINE); }

    mask     = canonicalized_path(mask);
    filename = canonicalized_path(filename);

    const char* cp   = nullptr;
    const char* mp   = nullptr;
    const char* wild = mask.c_str();
    const char* str  = filename.c_str();

    while ((*str) && (*wild != '*'))
    {
        if ((*wild != *str) && (*wild != '?')) { return 0; }
        ++wild;
        ++str;
    }

    while (*str)
    {
        if (*wild == '*') {
            if (!*++wild) { return 1; }
            mp = wild;
            cp = str + 1;
        }
        else if ((*wild == *str) || (*wild == '?')) {
            wild++;
            str++;
        }
        else {
            wild = mp;
            str  = cp++;
        }
    }

    while (*wild == '*') { wild++; }
    return !*wild;
}

/*
========================================
*/

#ifdef __linux__
std::string get_home_directory()
{
    const char* const ret = getenv("HOME");
    if (ret == nullptr) {
        struct passwd* const pw = getpwuid(geteuid());
        return pw->pw_dir;
    }
    else {
        return ret;
    }
}
#endif
