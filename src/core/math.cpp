#include "math.h"

#include "common.h"
#include "exception.h"

const hs_math_table hs_math;

/*
========================================
*/

hs_math_table::hs_math_table()
{
    pi           = std::atan(1.0f) * 4.0f;
    angle_factor = pi / 180.0f;

    //  Angle stuff
    for (int step = -3600; step <= 3600; ++step) {
        degrees_to_radians  [step + 3600] = (step / 10.0f) * (pi / 180.0f);
        sine_angle_degrees  [step + 3600] = std::sin(degrees_to_radians[step + 3600]);
        cosine_angle_degrees[step + 3600] = std::cos(degrees_to_radians[step + 3600]);
    }
}

/*
========================================
*/

float cosdeg (const float angle)
{
    return std::cos(deg_to_rad(angle));
//    return hs_math.cosine_angle_degrees[static_cast<unsigned int>((angle * 10) + 3600)];
}

/*
========================================
*/

float sindeg (const float angle)
{
    return std::sin(deg_to_rad(angle));
//    return hs_math.sine_angle_degrees[static_cast<unsigned int>((angle * 10) + 3600)];
}

/*
========================================
*/

float deg_to_rad (const float angle)
{
    return angle * hs_math.angle_factor;
}

/*
========================================
*/

float rad_to_deg (const float angle)
{
    return angle * (180.0f / hs_math.pi);
}

/*
========================================
*/

bool are_same (const float a, const float b)
{
    return (std::fabs(a - b) < std::numeric_limits<float>::epsilon());
}

/*
========================================
*/

int consecutive_sum (int min, int max)
{
    if (min > max) { std::swap(min, max); }
    return ((max - min) + 1) * (min + max) / 2;
}

/*
========================================
*/

int point_distance (const int x1, const int y1, const int x2, const int y2)
{
    return std::sqrt(
        std::pow(std::abs(x1 - x2), 2) +
        std::pow(std::abs(y1 - y2), 2));
}


float point_distance (
    const float x1, const float y1, const float z1,
    const float x2, const float y2, const float z2)
{
    return std::sqrt(
        std::pow(std::fabs(x1 - x2), 2.0f) +
        std::pow(std::fabs(y1 - y2), 2.0f) +
        std::pow(std::fabs(z1 - z2), 2.0f));
}

/*
========================================
*/

float fpoint_distance (
    const float x1, const float y1, const float x2, const float y2)
{
    return std::sqrt(std::pow(std::fabs(x1 - x2), 2.0f) + std::pow(std::fabs(y1 - y2), 2.0f));
}

/*
========================================
*/

bool line_intersect_slab (
    const float slab_min,   const float slab_max,
    const float line_start, const float line_end,
    float&      tb_enter,   float&      tb_exit)
{
	const float ray_dir = line_end - line_start;

	// ray parallel to the slab
	if (std::fabs(ray_dir) < std::numeric_limits<float>::epsilon()) {
		return range(line_start, slab_min, slab_max);
	}

	// slab's enter and exit parameters
	float ts_enter = (slab_min - line_start) / ray_dir;
	float ts_exit  = (slab_max - line_start) / ray_dir;
	if (ts_enter > ts_exit) { std::swap(ts_enter, ts_exit); }

	// make sure the slab interval and the current box intersection interval overlap
	const bool missed = (tb_enter > ts_exit || ts_enter > tb_exit);
	if (!missed) {
		tb_enter = std::max(tb_enter, ts_enter);
		tb_exit  = std::min(tb_exit, ts_exit);
	}
	return !missed;
}

/*
========================================
*/

float circle_arc_degrees (const float circle_radius, const float arc_length)
{
    if (circle_radius <= 0.0f || arc_length <= 0.0f) {
        throw core::exception(ERROR_FILE_LINE);
    }

    const float circumference = 2 * hs_math.pi * circle_radius;
    return (arc_length / circumference) * 360.0f;
}

/*
========================================
*/

float opposite_angle (const float angle)
{
    float result = angle + 180.0f;
    if (result > 360.0f) { result -= 360.0f; }
    return result;
}

/*
========================================
*/

bool angle_within_range (float angle_min, float angle_max, float target_angle)
{
    angle_max    = ((angle_max - angle_min) < 0.0f) ? angle_max - angle_min + 360.0f : angle_max - angle_min;
    target_angle = ((angle_max - angle_min) < 0.0f) ? target_angle - angle_min + 360.0f : target_angle - angle_min;
    angle_min = fnum_wrap(angle_min, 0.0f, 360.0f);
    angle_max = fnum_wrap(angle_max, 0.0f, 360.0f);
    target_angle = fnum_wrap(target_angle, 0.0f, 360.0f);
    return (target_angle < angle_max);
}

/*
========================================
*/

float angle_offset (const float angle, const float target_angle)
{
    const float pre_angle = target_angle - angle;
    return (pre_angle >  180.0f) ? pre_angle - 360.0f :
           (pre_angle < -180.0f) ? pre_angle + 360.0f : pre_angle;
}

/*
========================================
gmod()

Knuth-style modulo
========================================
*/

int gmod (const int lhs, const int rhs)
{
    return (rhs == 0) ? 0 : lhs - (rhs * std::floor(static_cast<float>(lhs) / rhs));
}

/*
========================================
*/

int next_power_of_two (const int value)
{
    /** Returns the next highest power-of-two, NOT including the value provided.
        (e.g. if you pass 2, it will return 4, not 2)   */

    int val = value + 1;

    --val;
    val = (val >>  1) | val;
    val = (val >>  2) | val;
    val = (val >>  4) | val;
    val = (val >>  8) | val;
    val = (val >> 16) | val;
    ++val;

    return val;
}

/*
========================================
*/

int get_power_of_two (int value)
{
    /** Returns the next highest power-of-two, INCLUDING the value provided.
        (e.g. if you pass 2, you'll get 2 back) */

    --value;
    value = (value >>  1) | value;
    value = (value >>  2) | value;
    value = (value >>  4) | value;
    value = (value >>  8) | value;
    value = (value >> 16) | value;
    ++value;

    return value;
}

/*
========================================
*/

int sgn (const float value)
{
    return (value  > 0.0f) ? +1 : (value < 0.0f) ? -1 : 0;
}

/*
========================================
*/

float frac (const float value)
{
    return value - static_cast<int>(value);
}

/*
========================================
*/

float lerp (const float value, float low, float high)
{
    if (low == high && value == low) { return 1.0f; }
    if (low > high) { std::swap(low, high); }
    return (value - low) / (high - low);
}


float rlerp (float low, float high, const float amount)
{
    if (low > high) { std::swap(low, high); }
    return low + ((high - low) * amount);
}

/*
========================================
*/

bool is_even (const int value)
{
    return (value % 2 == 0);
}

/*
========================================
*/

bool is_odd (const int value)
{
    return (value % 2 != 0);
}

/*
========================================
*/

int force_even (const int value)
{
    return ((value / 2) * 2);
}

/*
========================================
*/

int force_odd (const int value)
{
    return ((value / 2) * 2) + 1;
}
