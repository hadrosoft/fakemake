#ifndef HADROSOFT_CORE_IOFILE_H_INCLUDED
#define HADROSOFT_CORE_IOFILE_H_INCLUDED

#include <fstream>

#include "file_meta.h"
#include "exception.h"

class iofile
{
    public:
        iofile  (const std::string& filename_, File_Access amode, File_Data dmode);
        ~iofile ();

        //  Management
        std::string         filename    () const;
        size_t              file_size   () const;
        size_t              current_pos () const;
        void                seek        (size_t offset, File_Pos_Relative origin = File_Pos_Relative::Beginning) const;
        bool                eof         () const;
        void                set_mode    (File_Access new_access_mode, File_Data new_data_mode) const;
        void                set_mode    (File_Access new_access_mode) const;
        void                set_mode    (File_Data new_data_mode) const;

        //  Data output
        void                put                        (const void* buffer, size_t size, size_t count) const;
        void                put                        (const void* buffer, size_t byte_count) const;
        void                open_string_output_buffer  (size_t padding_size = 4) const;
        void                close_string_output_buffer () const;
        void                write_string               (const std::string& str) const;
        void                write_naked_string         (const std::string& str) const;
        void                write_bool                 (bool value) const;
        void                write_ubyte                (unsigned char value) const;
        void                write_char                 (char value) const;
        void                write_byte                 (char value) const;
        void                write_short                (short value) const;
        void                write_ushort               (unsigned short value) const;
        void                write_int                  (int value) const;
        void                write_uint                 (unsigned int value) const;
        void                write_size_t               (size_t value) const;
        void                write_long                 (long int value) const;
        void                write_ulong                (unsigned long int value) const;
        void                write_float                (float value) const;
        void                write_double               (double value) const;

        template <typename T> void write (T value) const
        {
            static_assert(std::is_arithmetic<T>::value || std::is_enum<T>::value,
                          "generic argument to 'iofile::write()' must be arithmetic type"
                          "(integral or floating point)  or an enumeration (integral constant)");
            put(&value, sizeof(T));
        }

        void                append_file (const std::string& source_filename) const;

        //  Data input
        void                get (void* buffer, size_t size, size_t count) const;
        void                get (void* buffer, size_t byte_count) const;

        std::string         read_string () const;
        void                read_string (std::string& target_string) const;
        std::string         read_string (size_t string_length) const;
        void                read_string (std::string& target_string, size_t string_length) const;
        std::string         line_input  () const;
        void                line_input  (std::string& target_string) const;

        void                read_bool (bool& target) const;
        bool                read_bool () const;

        void                read_ubyte (unsigned char& target) const;
        unsigned char       read_ubyte () const;

        void                read_char (char& target) const;
        char                read_char () const;

        void                read_byte (char& target) const;
        char                read_byte () const;

        void                read_short (short& target) const;
        short               read_short () const;

        void                read_ushort (unsigned short& target) const;
        unsigned short      read_ushort () const;

        void                read_int (int& target) const;
        int                 read_int () const;

        void                read_uint (unsigned int& target) const;
        unsigned int        read_uint () const;

        void                read_size_t (size_t& target) const;
        size_t              read_size_t () const;

        void                read_long (long int& target) const;
        long int            read_long () const;

        void                read_ulong (unsigned long int& target) const;
        unsigned long int   read_ulong () const;

        void                read_float (float& target) const;
        float               read_float () const;

        void                read_double (double& target) const;
        double              read_double () const;

        template <typename T> void read (T& target) const
        {
            static_assert(std::is_arithmetic<T>::value || std::is_enum<T>::value,
                          "generic argument to 'iofile::write()' must be "
                          "arithmetic type (integral or floating point) "
                          "or an enumeration (integral constant)");
            get(&target, sizeof(T));
        }

    private:
        void open (File_Access amode, File_Data dmode) const;
        void close() const;
        void write_ascii_string (const std::string& str) const;

        std::string          _filename;
        mutable std::fstream _file;
        mutable File_Access  _access_mode;
        mutable File_Data    _data_mode;

        mutable bool         _string_output_buffer_on  = false;
        mutable size_t       _string_output_buffer_pad = 4;
        mutable std::string  _string_output_buffer;
};

#endif      //  HADROSOFT_CORE_IOFILE_H_INCLUDED
