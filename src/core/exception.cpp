#include "exception.h"

/*
========================================
*/

core::exception::exception (const std::string& message) :
    _message (message)
    {}

/*
========================================
*/

std::string core::exception::what() const throw()
{
    return _message;
}
