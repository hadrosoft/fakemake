#include "file_directory.h"

#include <iostream>
#include <algorithm>

#include "system.h"
#include "exception.h"
#include "common.h"

/*
========================================
*/

directory_listing::directory_listing() {}


directory_listing::directory_listing (const directory_listing& source) :
    _path                 (source._path),
    _mask                 (source._mask),
    _file_list            (source._file_list),
    _include_directories  (source._include_directories),
    _file_item_count      (source._file_item_count),
    _directory_item_count (source._directory_item_count)
    {}


directory_listing::directory_listing (
    const std::string&  directory,
    const std::string&  file_mask           /*= "*.*"*/,
    const bool          include_directories /*= false*/) :
        _path                 (canonicalized_path(directory)),
        _mask                 (extract_filename(file_mask)),
        _include_directories  (include_directories)
    {
        load();
    }

/*
========================================
*/

void directory_listing::refresh()
{
    _file_list.clear();
    load();
}


void directory_listing::refresh (
    const std::string& directory, const std::string& file_mask)
{
    _path = canonicalized_path(get_directory(directory));
    _mask = extract_filename(file_mask);
    refresh();
}

/*
========================================
*/

void directory_listing::load()
{
    #if defined _WIN32 || defined _WIN64

        //--------------------------------------------------------------------//
        //  Windows                                                           //
        //--------------------------------------------------------------------//

        WIN32_FIND_DATA find_file_data;
        HANDLE          file_result;
        std::string     search_param = _path + "/" + _mask;
        File_Type       file_type;
        bool            read_only;

        file_result = FindFirstFile(search_param.c_str(), &find_file_data);

        //  List directories
        if (_include_directories) {
            file_result = FindFirstFile(std::string(_path + "/*.*").c_str(), &find_file_data);
            do {
                if (find_file_data.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM    ||
                    find_file_data.dwFileAttributes & FILE_ATTRIBUTE_TEMPORARY ||
                    find_file_data.dwFileAttributes & FILE_ATTRIBUTE_OFFLINE   ||
                    find_file_data.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN)
                {
                    continue;
                }

                if (find_file_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
                    file_type = File_Type::Directory;
                    read_only = (find_file_data.dwFileAttributes & FILE_ATTRIBUTE_READONLY) ? true : false;
                    _file_list.emplace_back(std::string(find_file_data.cFileName), File_Type::Directory, read_only);
                }
            } while (FindNextFile(file_result, &find_file_data) != 0);
            FindClose(file_result);
        }


        //  Find files that match the mask
        file_result = FindFirstFile(search_param.c_str(), &find_file_data);

        //  On fail, return an empty directory listing
        if (file_result == INVALID_HANDLE_VALUE) {
            FindClose(file_result);
            return;
        }

        do {
            if (find_file_data.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM)    { continue; }
            if (find_file_data.dwFileAttributes & FILE_ATTRIBUTE_TEMPORARY) { continue; }
            if (find_file_data.dwFileAttributes & FILE_ATTRIBUTE_OFFLINE)   { continue; }
            if (find_file_data.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN)    { continue; }

            file_type = (find_file_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) ? File_Type::Directory : File_Type::File;

            if (_include_directories && file_type == File_Type::Directory) {
                //  We already collected folders up top; skip this entry
                continue;
            }

            read_only = (find_file_data.dwFileAttributes & FILE_ATTRIBUTE_READONLY) ? true : false;

            _file_list.emplace_back(std::string(find_file_data.cFileName), file_type, read_only);
        } while (FindNextFile(file_result, &find_file_data) != 0);

        FindClose(file_result);

    #else
        if (!fs::is_directory(_path)) { return; }

        _file_list.clear();

        //  List directories
        if (_include_directories) {
            for (const fs::directory_entry& p: fs::directory_iterator(_path)) {
                if (!fs::is_directory(p)) { continue; }
                const std::string filename   = p.path().string();
                const std::string short_name = extract_filename(filename);
                if ((extract_filename(filename)).front() == '.') { continue; }
                if (fs::is_empty(p)) { continue; }
                _file_list.emplace_back(short_name, File_Type::Directory, true);
            }
        }

        //  Get files
        const std::string full_path_mask = canonicalized_path(_path + "/" + _mask);
        for (const fs::directory_entry& p: fs::directory_iterator(_path)) {
            if (fs::is_directory(p)) { continue; }
            const std::string filename   = p.path().string();
            const std::string short_name = extract_filename(filename);
            if (short_name.front() == '.') { continue; }
            if (filename_wildcard_match(full_path_mask, filename)) {
                const File_Type file_type = (fs::is_directory(p)) ? File_Type::Directory : File_Type::File;
                const bool read_only = !access(filename.c_str(), R_OK);
                _file_list.emplace_back(short_name, file_type, read_only);
            }
        }
    #endif

    sort();
    count_items();
}

/*
========================================
*/

void directory_listing::sort()
{
    if (_file_list.size() < 2) { return; }

    std::vector<file_info> dir_hold;
    std::vector<file_info> file_hold;

    for (const file_info& entry: _file_list) {
        if (entry.type() == File_Type::File) {
            file_hold.push_back(entry);
        }
        else if (entry.type() == File_Type::Directory) {
            dir_hold.push_back(entry);
        }
    }

    std::sort(dir_hold.begin(), dir_hold.end());
    std::sort(file_hold.begin(), file_hold.end());

    _file_list.clear();
    _file_list.insert(_file_list.end(), dir_hold.begin(), dir_hold.end());
    _file_list.insert(_file_list.end(), file_hold.begin(), file_hold.end());
}

/*
========================================
*/

std::string directory_listing::get_full_filename (const size_t index) const
{
    if (_file_list.empty() || index > _file_list.size() - 1) {
        throw core::exception(ERROR_FILE_LINE);
    }
    return canonicalized_path(_path + "/" + _file_list[index]._filename);
}


std::string directory_listing::get_full_filename (const file_info& file) const
{
    return canonicalized_path(_path + "/" + file._filename);
}

/*
========================================
*/

void directory_listing::count_items()
{
    _file_item_count      = 0;
    _directory_item_count = 0;

    for (const file_info& entry: _file_list) {
        if (entry.type() == File_Type::File) {
            ++_file_item_count;
        }
        else if (entry.type() == File_Type::Directory) {
            ++_directory_item_count;
        }
    }
}

/*
========================================
*/

void directory_listing::add_file (const file_info& file)
{
    _file_list.push_back(file);
}

/*
========================================
*/

void directory_listing::remove_file (const size_t index)
{
    if (index > _file_list.size() - 1) { throw core::exception(ERROR_FILE_LINE); }
    _file_list.erase(_file_list.begin() + index);
}


void directory_listing::remove_file (const file_info& file)
{
    _file_list.erase(std::remove(_file_list.begin(), _file_list.end(), file), _file_list.end());
}

/*
========================================
*/

std::string directory_listing::path() const
{
    return _path;
}

/*
========================================
*/

std::string directory_listing::mask() const
{
    return _mask;
}

/*
========================================
*/

std::string directory_listing::mask_full_path() const
{
    return canonicalized_path(_path + "/" + _mask);
}

/*
========================================
*/

size_t directory_listing::size() const
{
    return _file_list.size();
}

/*
========================================
*/

bool directory_listing::is_empty() const
{
    return _file_list.empty();
}

/*
========================================
*/

const file_info& directory_listing::operator [] (size_t index) const
{
    return _file_list[index];
}

/*
========================================
*/

size_t directory_listing::file_item_count() const
{
    return _file_item_count;
}

/*
========================================
*/

size_t directory_listing::directory_item_count() const
{
    return _directory_item_count;
}

/*
========================================
*/

std::vector<file_info>::const_iterator directory_listing::iterator() const
{
    return _file_list.begin();
}

/*
========================================
*/

std::vector<file_info>::const_iterator directory_listing::begin() const
{
    return _file_list.begin();
}

/*
========================================
*/

std::vector<file_info>::const_iterator directory_listing::end() const
{
    return _file_list.end();
}

/*
========================================
*/

std::vector<std::string> directory_listing::full_file_list (
    const bool include_directories /*= false*/) const
{
    std::vector<std::string> vec;
    for (const file_info& file: _file_list) {
        if (!include_directories && file.type() == File_Type::Directory) {
            continue;
        }
        vec.push_back(get_full_filename(file));
    }
    return vec;
}

/*
========================================
*/

std::vector<std::string> directory_listing::full_recursive_file_list (
    const bool include_directories /*= false*/) const
{
    const directory_listing working (_path, _mask, true);

    std::vector<std::string> files;
    std::vector<std::string> directories   {_path};

    while (!directories.empty())
    {
        const std::string              dir       = directories.back();
        const directory_listing        listing     (dir, _mask, true);
        const std::vector<std::string> filenames = listing.full_file_list(true);

        if (include_directories) { files.push_back(_path); }
        directories.pop_back();

        for (const std::string& str: filenames) {
            const File_Type type = file_type(str);
            if (type == File_Type::File) {
                files.push_back(str);
            }
            else if (type == File_Type::Directory) {
                if (include_directories) { files.push_back(str); }
                directories.push_back(str);
            }
        }
    }

    remove_duplicates(files);
    return files;
}

/*
========================================
*/

void directory_listing::report() const
{
    std::cout << std::endl << std::endl;
    std::cout << "========================" << std::endl;
    std::cout << "Directory listing report" << std::endl;
    std::cout << _path << "/" << _mask << std::endl;
    std::cout << "------------------------" << std::endl;

    if (_file_list.empty()) {
        std::cout << "No files to list." << std::endl;
    }
    else {
        for (const file_info& file: _file_list) {
            std::string message = "Type: ";

            if (file.type() == File_Type::Directory) {
                message += "Directory";
            }
            else if (file.type() == File_Type::File) {
                message += "File     ";
            }

            message += " | " + canonicalized_path(_path + "/" + file._filename);
            std::cout << message << std::endl;
        }
    }

    std::cout << "========================" << std::endl << std::endl << std::endl;
}
