#ifndef HADROSOFT_CORE_STRING_H_INCLUDED
#define HADROSOFT_CORE_STRING_H_INCLUDED

#include <string>
#include <cmath>
#include <vector>
#include <limits>
#include <sstream>
#include <stdexcept>

enum class String_Case
{
    Insensitive,
    Sensitive
};

using stringvec = std::vector<std::string>;
using quoteret  = std::pair<std::string, size_t>;

bool         is_newline                (char c);
bool         is_space                  (char c);
bool         is_dash                   (char c);
bool         is_number                 (const std::string& str);

std::string  string_word               (const std::string& str, size_t word_number);
size_t       string_word_char_pos      (const std::string& str, size_t word_number);
size_t       count_words_in_string     (const std::string& str);
size_t       count_lines_in_string     (const std::string& str);
stringvec    all_lines_in_string       (const std::string& str);
stringvec    all_words_in_string       (const std::string& str);

std::string  ucase                     (const std::string& str);
std::string  lcase                     (const std::string& str);
std::string  initial_capital           (std::string str);

std::string  ltrim                     (const std::string& str, const std::string& trim_chars = " \t");
std::string  rtrim                     (const std::string& str, const std::string& trim_chars = " \t");
std::string  trim                      (const std::string& str, const std::string& trim_chars = " \t");

stringvec    wrap_string_to_char_width (const std::string& str, size_t width);

std::string  safe_substr               (const std::string& str, size_t pos = 0, size_t n = std::string::npos);
std::string  safe_substr_range         (const std::string& str, size_t first_char_pos, size_t last_char_pos);
std::string  safe_substr_to_char       (const std::string& str, size_t first_char_pos, char find_char);
std::string  safe_substr_to_chars      (const std::string& str, size_t first_char_pos, const std::string& find_any_char);


bool         string_contains_char      (const std::string& str, char char_to_find);
bool         string_contains_any_char  (const std::string& str, const std::string& chars);
bool         string_contains_substring (const std::string& str, const std::string& substring, bool case_sensitive = false);
bool         string_match              (const std::string& str, size_t search_pos, const std::string& search_str, bool case_sensitive = true);

void         string_replace            (std::string& str, const std::string& check_str, const std::string& replace_str);
std::string  string_replace            (const std::string& str, const std::string& check_str, const std::string& replace_str);
void         string_replace            (std::string& str, size_t start_pos, size_t end_pos, const std::string& replace_str);
std::string  string_replace            (const std::string& str, size_t start_pos, size_t end_pos, const std::string& replace_str);

void         strip_chars               (std::string& str, const std::string& chars_to_remove);
std::string  strip_chars               (const std::string& str, const std::string& chars_to_remove);
void         strip_repeated_chars      (std::string& str, const std::string& chars_to_remove);
std::string  strip_repeated_chars      (const std::string& str, const std::string& chars_to_remove);
void         strip_repeated_whitespace (std::string& str);
std::string  strip_repeated_whitespace (const std::string& str);
double       strval                    (const std::string& str);
bool         bool_from_token           (std::string token);
std::string  space_pad_back            (const std::string& str, size_t char_count);
stringvec    split_string              (const std::string& str, char delimiter);
std::string  intstr                    (int val);
std::string  numstr_separators         (float val);
std::string  intstr_separators         (int val);
quoteret     read_quoted_string        (const std::string& str, size_t start_at_word = 1);
std::string  get_property_from_string  (const std::string& line, const std::string& property_name, size_t number_of_tokens = 1);
std::string  letter_sequence           (int index);
stringvec    text_columns              (const std::vector<std::string>& string_list, size_t max_table_width = 80, size_t column_padding = 1);

/*
========================================
*/

template <typename T> std::string numstr (
    const T         val,
    const size_t    decimal_places = std::numeric_limits<size_t>::max())
{
    #define SIZE_T_MAX std::numeric_limits<size_t>::max()

    static_assert(std::is_arithmetic<T>::value ||
                  std::is_enum<T>::value,
                  "argument to 'numstr' must be numeric value");

    std::stringstream stream;
    if (decimal_places != SIZE_T_MAX) {
        stream.precision(decimal_places + intstr(val).size());
    }

    if (std::is_same<T, bool>::value) {
        stream << std::boolalpha << val;
    }
    else {
        stream << val;
    }

    return stream.str();
}


template <typename T> std::string numstr (T* const val)
{
    std::stringstream stream;
    stream << val;
    return stream.str();
}

/*
========================================
*/

template <typename T>
std::string zero_pad (const T val, const size_t digit_count)
{
    static_assert(std::is_integral<T>::value,
                  "argument to 'zero_pad' must be integer value");

    std::string ret = intstr(val);
    if (digit_count > ret.size()) {
        ret.insert(0, digit_count - ret.size(), '0');
    }
    return ret;
}

/*
========================================
*/

template <typename T>
std::string space_pad (const T val, const size_t char_count)
{
    static_assert(std::is_integral<T>::value,
                  "argument to 'space_pad' must be integer value");

    std::string ret = intstr(val);
    if (char_count > ret.size()) {
        ret.insert(0, char_count - ret.size(), ' ');
    }
    return ret;
}

#endif      //  HADROSOFT_CORE_STRING_H_INCLUDED
