#ifndef HADROSOFT_CORE_SYSTEM_H_INCLUDED
#define HADROSOFT_CORE_SYSTEM_H_INCLUDED

#if defined _WIN32 || defined _WIN64
    #include <Windows.h>
    #include <Shlobj.h>
    #include <Lmcons.h>
#else
    #include <experimental/filesystem>
    namespace fs = std::experimental::filesystem::v1;
    #include <fstream>
    #include <limits.h>
    #include <unistd.h>
#endif

#include <string>

extern std::string exepath;

std::string exe_name();
bool        get_exepath();

#endif      //  HADROSOFT_CORE_SYSTEM_H_INCLUDED
