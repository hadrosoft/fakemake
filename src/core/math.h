#ifndef HADROSOFT_CORE_MATH_H_INCLUDED
#define HADROSOFT_CORE_MATH_H_INCLUDED

#include <type_traits>
#include <algorithm>
#include <cmath>
#include <vector>
#include <cstdint>

template <typename T> class array2d;

/*
========================================
*/

struct hs_math_table
{
    hs_math_table();

    float degrees_to_radians   [7201];
    float sine_angle_degrees   [7201];
    float cosine_angle_degrees [7201];

    float pi;
    float angle_factor;
} extern const hs_math;

//------------------------------------------------------------------------------

bool  are_same            (float a, float b);
int   consecutive_sum     (int min, int max);
int   point_distance      (int x1, int y1, int x2, int y2);
float point_distance      (float x1, float y1, float z1, float x2, float y2, float z2);
float fpoint_distance     (float x1, float y1, float x2, float y2);
bool  line_intersect_slab (float slab_min, float slab_max, float line_start, float line_end, float& tb_enter, float& tb_exit);
float circle_arc_degrees  (float circle_radius, float arc_length);
float cosdeg              (float angle);
float sindeg              (float angle);
float deg_to_rad          (float angle);
float rad_to_deg          (float angle);
float opposite_angle      (float angle);
bool  angle_within_range  (float angle_min, float angle_max, float target_angle);
float angle_offset        (float angle, float target_angle);
int   gmod                (int lhs, int rhs);
int   next_power_of_two   (int value);
int   get_power_of_two    (int value);
int   sgn                 (float value);
float frac                (float value);
float lerp                (float value, float low, float high);
float rlerp               (float low, float high, float amount);
bool  is_even             (int value);
bool  is_odd              (int value);
int   force_even          (int value);
int   force_odd           (int value);

/*
========================================
*/

template <typename T> void clamp (T& value, float low, float high)
{
    static_assert(std::is_arithmetic<T>::value,
                  "argument to 'clamp' must be arithmetic type (integral or "
                  "floating point)");
    if (low > high) { std::swap(low, high); }
    value = (value < low) ? low : (value > high) ? high : value;
}

/*
========================================
*/

template <typename T> void clamp_abs (T& value, const float limit)
{
    static_assert(std::is_arithmetic<T>::value,
                  "argument to 'clamp' must be arithmetic type (integral or "
                  "floating point)");

    if (std::abs(value) > std::abs(limit)) { value = std::abs(limit) * sgn(value); }
}

/*
========================================
*/

template <typename T> T clamped (T value, const float low, const float high)
{
    clamp(value, low, high);
    return value;
}

/*
========================================
*/

template <typename T> T clamped_abs (T value, const float limit)
{
    clamp_abs(value, limit);
    return value;
}

/*
========================================
*/

template <typename T> void round_to_increment (T& val, const float increment)
{
    static_assert(std::is_arithmetic<T>::value,
                  "argument to 'round_to_increment' must be arithmetic type "
                  "(integral or floating point)");
    T inc = static_cast<T>(increment);
    val = std::ceil(static_cast<float>(val) / inc) * inc;
}

/*
========================================
*/

template <typename T> T rounded_to_increment (T val, const float increment)
{
    round_to_increment(val, increment);
    return val;
}

#endif      //  HADROSOFT_CORE_MATH_H_INCLUDED
