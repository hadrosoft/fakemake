#ifndef HADROSOFT_CORE_EXCEPTION_H_INCLUDED
#define HADROSOFT_CORE_EXCEPTION_H_INCLUDED

#include <string>

#define STRINGIZE_DETAIL(x) #x
#define STRINGIZE(x)        STRINGIZE_DETAIL(x)
#define ERROR_FILE_LINE     std::string(STRINGIZE(__FILE__)) + " (" + std::string(STRINGIZE(__LINE__)) + ") - " + __PRETTY_FUNCTION__
#define LINE_NUMBER         std::string(STRINGIZE(__LINE__))

namespace core
{
    class exception
    {
        public:
            exception (const std::string& message);
            std::string what() const throw();

        private:
            std::string _message;
    };
}

#endif      //  HADROSOFT_CORE_EXCEPTION_H_INCLUDED
