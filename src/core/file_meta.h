#ifndef HADROSOFT_CORE_FILE_META_H_INCLUDED
#define HADROSOFT_CORE_FILE_META_H_INCLUDED

/*
========================================
*/

enum class File_Access
{
    Input,
    Output,
    Append,
    Random,
    Random_Append
};

/*
========================================
*/

enum class File_Data
{
    Ascii,
    Binary
};

/*
========================================
*/

enum class File_Pos_Relative
{
    Beginning,
    Current,
    Eof
};

#endif      //  HADROSOFT_CORE_FILE_META_H_INCLUDED
