#include "common.h"

#include <chrono>
#include <map>
#include <random>

#include "string.h"
#include "math.h"

/*
========================================
*/

bool range (const float value, float min, float max)
{
    if (min > max) { std::swap(min, max); }
    return (value >= min && value <= max);
}

/*
========================================
fnum_wrap()

Overflow on an input value in the range [min, max)
========================================
*/

int num_wrap (const int value, int min, int max)
{
    max -= 1;
    if (min > max) { std::swap(min, max); }
    return (value >= min && value <= max) ?
        value :
        gmod((value - min), (max - min + 1)) + min;
}


float fnum_wrap (const float value, float min, float max)
{
    if (min > max) { std::swap(min, max); }
    float t = (value - min) / (max - min);
    return min + (max - min) * (t - std::floor(t));
}

/*
========================================
*/

std::mt19937 twister (std::chrono::high_resolution_clock::now().time_since_epoch().count());

int rand_int (int min, int max)
{
    if (min > max) { std::swap(min, max); }
    std::uniform_int_distribution<int> dist(min, max);
    return dist(twister);
}

/*
========================================
*/

float random_num (float min, float max)
{
    if (min > max) { std::swap(min, max); }
    std::uniform_real_distribution<float> dist(min, std::nextafter(max, std::numeric_limits<float>::max()));
    return dist(twister);
}

/*
========================================
*/

static std::chrono::time_point<std::chrono::high_resolution_clock> timer_start;

void init_timer()
{
    timer_start = std::chrono::high_resolution_clock::now();
}

/*
========================================
*/

double timer()
{
    const auto current_time = std::chrono::high_resolution_clock::now();
    const auto duration     = std::chrono::duration_cast<std::chrono::duration<double>>(current_time - timer_start);
    return duration.count();
}

/*
========================================
*/

std::string current_date_display (
    const bool  month_word,
    const bool  pad_numbers,
    const bool  short_name  /*= false*/)
{
    const std::time_t t   = std::time(nullptr);
    std::tm* const    now = std::localtime(&t);

    const std::string day          = (pad_numbers) ? zero_pad(now->tm_mday, 2) : intstr(now->tm_mday);
    const std::string year         = intstr(now->tm_year + 1900);
    const int         month_number = now->tm_mon + 1;

    if (month_word) {
        static const std::vector<std::string> month_names_long
        {
            "January", "February", "March",     "April",   "May",      "June",
            "July",    "August",   "September", "October", "November", "December"
        };
        static const std::vector<std::string> month_names_short
        {
            "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        };

        const std::vector<std::string>& vec = (short_name) ? month_names_short : month_names_long;
        return day + " " + vec[month_number - 1] + " " + year;
    }
    else {
        const std::string month = (pad_numbers) ? zero_pad(month_number, 2) : intstr(month_number);
        return year + "-" + month + "-" + day;
    }
}

/*
========================================
*/

std::string current_time_display()
{
    const std::time_t t   = std::time(nullptr);
    std::tm* const    now = std::localtime(&t);

    return zero_pad(now->tm_hour, 2) + ":" + zero_pad(now->tm_min, 2) + ":" + zero_pad(now->tm_sec, 2);
}

/*
========================================
*/

std::string current_time()
{
    time_t     raw_time;
    struct tm* time_info;

    time(&raw_time);
    time_info = localtime(&raw_time);

    return asctime(time_info);
}

/*
========================================
*/

bool overlap (int x1, int x2, int y1, int y2)
{
    if (x1 > x2) { std::swap(x1, x2); }
    if (y1 > y2) { std::swap(y1, y2); }
    return (x1 <= y2 && y1 <= x2);
}


bool overlap (const std::pair<int, int>& pair1, const std::pair<int, int>& pair2)
{
    return overlap(pair1.first, pair1.second, pair2.first, pair2.second);
}

/*
========================================
*/

std::string seconds_to_time_display (const float seconds)
{
    int total_time = seconds;

    const unsigned int hour = total_time / 3600;
    total_time %= 3600;
    const unsigned int min = total_time / 60;
    total_time %= 60;
    const unsigned int sec = total_time;

    return ((hour > 0) ? numstr(hour) + ":" : "") +
           ((hour > 0) ? zero_pad(min, 2) : numstr(min)) + ":" +
           zero_pad(sec, 2);
}
