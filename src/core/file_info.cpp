#include "file_info.h"

#include "string.h"

/*
========================================
*/

file_info::file_info (const file_info& source) :
    _filename  (source._filename),
    _type      (source._type),
    _read_only (source._read_only)
    {}


file_info::file_info (
    const std::string& filename_,const File_Type type_, const bool read_only_) :
        _filename  (filename_),
        _type      (type_),
        _read_only (read_only_)
    {}

/*
========================================
*/

std::string file_info::filename() const
{
    return _filename;
}

/*
========================================
*/

File_Type file_info::type() const
{
    return _type;
}

/*
========================================
*/

bool file_info::read_only() const
{
    return _read_only;
}

//------------------------------------------------------------------------------

/*
========================================
*/

bool operator == (const file_info& lhs, const file_info& rhs)
{
    return (lhs.filename()  == rhs.filename() &&
            lhs.type()      == rhs.type() &&
            lhs.read_only() == rhs.read_only());
}

/*
========================================
*/

bool operator < (const file_info& lhs, const file_info& rhs)
{
    return (lcase(lhs.filename()) < lcase(rhs.filename()));
}

/*
========================================
*/

bool operator > (const file_info& lhs, const file_info& rhs)
{
    return (lcase(lhs.filename()) > lcase(rhs.filename()));
}
