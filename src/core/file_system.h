#ifndef HADROSOFT_CORE_FILE_SYSTEM_H_INCLUDED
#define HADROSOFT_CORE_FILE_SYSTEM_H_INCLUDED

#include <string>

enum class File_Type
{
    File,
    Directory,
    Does_Not_Exist
};

bool        file_exists              (const std::string& filename);
bool        directory_exists         (const std::string& directory_name, bool make_on_fail = false);
std::string get_directory            (const std::string& path_name);
bool        create_directory         (const std::string& directory_name);
std::string extract_filename         (const std::string& path_name);
std::string strip_file_extension     (const std::string& filename);
bool        kill_file                (const std::string& filename, bool backup_first = false, std::string* backup_filename = nullptr);
bool        copy_file                (const std::string& source_filename, const std::string& new_filename, bool overwrite_if_exists);
bool        rename_file              (const std::string& old_filename, const std::string& new_filename);
size_t      file_size                (const std::string& filename);
File_Type   file_type                (const std::string& filename);
std::string canonicalized_path       (std::string path_name);
bool        file_is_safe_for_writing (const std::string& filename);
bool        filename_wildcard_match  (std::string mask, std::string filename);

#ifdef __linux__
std::string get_home_directory();
#endif

#endif      //  HADROSOFT_CORE_FILE_SYSTEM_H_INCLUDED
