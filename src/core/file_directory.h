#ifndef HADROSOFT_CORE_FILE_DIRECTORY_H_INCLUDED
#define HADROSOFT_CORE_FILE_DIRECTORY_H_INCLUDED

#include <string>
#include <vector>

#include "file_info.h"

class directory_listing
{
    public:
        directory_listing ();
        directory_listing (const directory_listing& source);
        directory_listing (const std::string& directory, const std::string& file_mask = "*.*",
                           bool include_directories = false);

        //  Info
        std::string      path() const;
        std::string      mask() const;
        std::string      mask_full_path() const;
        size_t           size() const;
        bool             is_empty() const;
        const file_info& operator [] (size_t index) const;
        std::string      get_full_filename (size_t index) const;
        std::string      get_full_filename (const file_info& file) const;
        size_t           file_item_count() const;
        size_t           directory_item_count() const;

        //  Manipulation
        void             sort();
        void             add_file (const file_info& file);
        void             remove_file (size_t index);
        void             remove_file (const file_info& file);
        void             count_items();

        void             refresh();
        void             refresh (const std::string& directory, const std::string& file_mask);

        std::vector<file_info>::const_iterator iterator() const;
        std::vector<file_info>::const_iterator begin() const;
        std::vector<file_info>::const_iterator end() const;

        std::vector<std::string> full_file_list (bool include_directories = false) const;
        std::vector<std::string> full_recursive_file_list (bool include_directories = false) const;

        void report() const;

    private:
        void load();

        std::string            _path;
        std::string            _mask;
        std::vector<file_info> _file_list;
        bool                   _include_directories  = false;

        size_t                 _file_item_count      = 0;
        size_t                 _directory_item_count = 0;
};

#endif      //  HADROSOFT_CORE_FILE_DIRECTORY_H_INCLUDED
