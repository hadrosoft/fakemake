#ifndef HADROSOFT_CORE_COMMON_H_INCLUDED
#define HADROSOFT_CORE_COMMON_H_INCLUDED

#include <algorithm>
#include <memory>
#include <vector>
#include <deque>
#include <numeric>
#include <string>

#define FLOAT_MIN    std::numeric_limits<float>::min()
#define FLOAT_MAX    std::numeric_limits<float>::max()
#define EMPLACE_TYPE template <typename T, typename... Args>
#define EMPLACE_ARGS template <typename... Args>

bool        range                   (float value, float min, float max);
int         num_wrap                (int value, int min, int max);             //  Overflow on an input value in the range [min, max)
float       fnum_wrap               (float value, float min, float max);       //  [min, max)
int         rand_int                (int min, int max);
float       random_num              (float min, float max);
void        init_timer              ();
double      timer                   ();
std::string current_date_display    (bool month_word, bool pad_numbers, bool short_name = false);
std::string current_time_display    ();
bool        overlap                 (int x1, int x2, int y1, int y2);
bool        overlap                 (const std::pair<int, int>& pair1, const std::pair<int, int>& pair2);
std::string current_time            ();
std::string seconds_to_time_display (float seconds);

/*
========================================
*/

template <typename T, typename R> size_t fake_map_find (
    const std::vector<std::pair<T, R>>& vec, const T& key)
{
    for (size_t index = 0; index < vec.size(); ++index) {
        if (vec[index].first == key) { return index; }
    }
    return vec.size();
}

/*
========================================
*/

template <typename T> void remove_duplicates (std::vector<T>& vec)
{
    std::sort(vec.begin(), vec.end());
    vec.erase(std::unique(vec.begin(), vec.end()), vec.end());
}

/*
========================================
*/

template <typename T> void remove_item (std::vector<T>& vec, const T& item)
{
    for (auto it = vec.begin(); it != vec.end();) {
        if (item == *it) {
            it = vec.erase(it);
        }
        else {
            ++it;
        }
    }
}

/*
========================================
*/

template <typename T> bool vector_contains_value (
    const std::vector<T>& vec, const T& val)
{
    return (std::find(vec.begin(), vec.end(), val) != vec.end());
}

/*
========================================
*/

template <typename T> bool vector_contains_item (
    const std::vector<T>& vec, const T& item)
{
    const auto p = [&item](const T& check){ return (&check == &item); };
    return (std::find_if(vec.begin(), vec.end(), p) != vec.end());
}

/*
========================================
*/

template <typename T, typename Predicate>
bool vector_contains (const std::vector<T>& vec, const Predicate& p)
{
    return (std::find_if(vec.begin(), vec.end(), p) != vec.end());
}

/*
========================================
*/

template <typename T, typename Predicate>
void remove_from_vector (std::vector<T>& vec, const Predicate& p)
{
    vec.erase(std::remove_if(vec.begin(), vec.end(), p), vec.end());
}


template <typename T, typename Predicate>
void remove_from_deque (std::deque<T>& vec, const Predicate& p)
{
    vec.erase(std::remove_if(vec.begin(), vec.end(), p), vec.end());
}

/*
========================================
*/

template <typename T> float vector_average (const std::vector<T>& vec)
{
    static_assert(std::is_arithmetic<T>::value,
                  "generic argument to 'vector_average()' must be "
                  "arithmetic type (integral or floating point)");

    return static_cast<float>(std::accumulate(vec.begin(), vec.end(), 0.0f)) / vec.size();
}

/*
========================================
*/

template <typename T> float vector_sum (const std::vector<T>& vec)
{
    static_assert(std::is_arithmetic<T>::value,
                  "generic argument to 'vector_average()' must be "
                  "arithmetic type (integral or floating point)");

    T ret = 0;
    for (const T& t: vec) { ret += t; }
    return ret;
}

/*
========================================
*/

template <typename T, typename Predicate>
const T* find_in_vector (const std::vector<T>& vec, const Predicate& p)
{
    const auto it = std::find_if(vec.begin(), vec.end(), p);
    return (it != vec.end()) ? &(*it) : nullptr;
}


template <typename T, typename Predicate>
const T* find_in_deque (const std::deque<T>& vec, const Predicate& p)
{
    const auto it = std::find_if(vec.begin(), vec.end(), p);
    return (it != vec.end()) ? &(*it) : nullptr;
}

/*
========================================
*/

template <typename T>
size_t vector_index (const std::vector<T>& vec, const T& item)
{
    const auto p  = [&item](const T& check) { return (&check == &item); };
    const auto it = std::find_if(vec.begin(), vec.end(), p);
    return (it != vec.end()) ? it - vec.begin() : std::string::npos;
}

/*
========================================
*/

template <typename T>
size_t vector_index_by_value (const std::vector<T>& vec, const T& item)
{
    for (size_t index = 0; index < vec.size(); ++index) {
        if (vec[index] == item) { return index; }
    }
    return std::string::npos;
}

/*
========================================
*/

template <typename T> void set_bit (T& val, const size_t bit_index)
{
    static_assert(std::is_integral<T>::value,
                  "generic argument to 'set_bit()' must be integral type");

    val |= (1 << bit_index);
}

/*
========================================
*/

template <typename T> void clear_bit (T& val, const size_t bit_index)
{
    static_assert(std::is_integral<T>::value,
                  "generic argument to 'clear_bit()' must be integral type");

    val &= ~(1 << bit_index);
}

/*
========================================
*/

template <typename T> void manipulate_bit (
    T& val, const size_t bit_index, const bool bit_value)
{
    static_assert(std::is_integral<T>::value,
                  "generic argument to 'manipulate_bit()' must be integral type");

    if (bit_value) {
        set_bit(val, bit_index);
    }
    else {
        clear_bit(val, bit_index);
    }
}

/*
========================================
*/

template <typename T> void toggle_bit (T& val, const size_t bit_index)
{
    static_assert(std::is_integral<T>::value,
                  "generic argument to 'toggle_bit()' must be integral type");

    val ^= (1 << bit_index);
}

/*
========================================
*/

template <typename T> bool check_bit (const T val, const size_t bit_index)
{
    static_assert(std::is_integral<T>::value,
                  "generic argument to 'check_bit()' must be integral type");

    return val & (1 << bit_index);
}

/*
========================================
*/

template <typename T> class is_cloneable
{
    using one = char;
    using two = long;

    template <typename C> static one test(__typeof__(&C::clone) ) ;
    template <typename C> static two test(...);

    public:
        enum { value = sizeof(test<T>(0)) == sizeof(char) };
};

#endif          //  HADROSOFT_CORE_COMMON_H_INCLUDED
