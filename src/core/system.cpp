#include <sstream>

#ifdef __linux__
    #include <pwd.h>
#endif

#include "system.h"
#include "file_system.h"

std::string exepath;

/*
========================================
*/

std::string exe_name()
{
    #ifdef TARGET_OS_MAC
        #error macOS unsupported yet!
    #elif defined __linux__
        char result[PATH_MAX];
        const ssize_t count = readlink("/proc/self/exe", result, PATH_MAX);
        return extract_filename(canonicalized_path(std::string(result, (count > 0) ? count : 0)));
    #elif defined _WIN32 || defined _WIN64
        char buffer[MAX_PATH];
        GetModuleFileName(nullptr, buffer, sizeof(buffer));

        std::stringstream stream;
        stream << buffer;
        return extract_filename(stream.str());
    #else
        #error Unknown platform! Currently only Win32/64 and Linux are supported.
    #endif

    return "";
}

/*
========================================
*/

bool get_exepath()
{
    #ifdef TARGET_OS_MAC
        #error macOS unsupported yet!
    #elif defined __linux__
        char          result   [PATH_MAX];
        const ssize_t count  = readlink("/proc/self/exe", result, PATH_MAX);
        std::string   ret    = canonicalized_path(std::string(result, (count > 0) ? count : 0));
        exepath              = fs::path(ret).remove_filename();
        return (count > 0);
    #elif defined _WIN32 || defined _WIN64
        char buffer[MAX_PATH];
        GetModuleFileName(nullptr, buffer, sizeof(buffer));

        std::stringstream stream;
        stream << buffer;
        exepath = stream.str();

        std::string target_str;

        for (int char_pos = exepath.size() - 1; char_pos > 0; --char_pos) {
            target_str = exepath.substr(char_pos, 1);

            if (target_str == "\\" || target_str == "/") {
                exepath = exepath.substr(0, char_pos);
                break;
            }
        }

        exepath = canonicalized_path(exepath);
        if (exepath.size() > 3) { return true; }
    #else
        #error Unknown platform! Currently only Win32/64 and Linux are supported.
    #endif

    return false;
}
