#include "iofile.h"

#include <iostream>

#include "file_system.h"
#include "string.h"

/*
========================================
*/

iofile::iofile (
    const std::string& filename_,
    const File_Access  amode,
    const File_Data    dmode) :
        _filename       (canonicalized_path(filename_)),
        _access_mode    (amode),
        _data_mode      (dmode)
    {
        open(_access_mode, _data_mode);
    }

/*
========================================
*/

iofile::~iofile()
{
    close();
}

/*
========================================
*/

void iofile::open (const File_Access amode, const File_Data dmode) const
{
    std::ios_base::openmode mode;

    if (amode == File_Access::Input) {
        mode = std::ios_base::in;
    }
    else if (amode == File_Access::Output) {
        mode = std::ios_base::out;
    }
    else if (amode == File_Access::Random ||
             amode == File_Access::Random_Append)
    {
        mode = std::ios_base::out | std::ios_base::in;
    }

    if (amode == File_Access::Append || amode == File_Access::Random_Append) {
        mode |= std::ios_base::app;
    }

    if (dmode == File_Data::Binary || amode == File_Access::Input) {
        mode |= std::ios_base::binary;
    }

    _file.open(_filename, mode);
    _access_mode = amode;
    _data_mode   = dmode;
}

/*
========================================
*/

void iofile::close() const
{
    _file.close();
}

/*
========================================
*/

std::string iofile::filename() const
{
    return _filename;
}

/*
========================================
*/

size_t iofile::file_size() const
{
    return ::file_size(_filename);
}

/*
========================================
*/

size_t iofile::current_pos() const
{
    const auto ret = _file.tellg();
    if (ret == std::fstream::pos_type(-1)) { return file_size(); }
    return ret;
}

/*
========================================
*/

void iofile::seek (
    const size_t            offset,
    const File_Pos_Relative origin  /*= File_Pos_Relative::Beginning*/) const
{
    const auto seek_origin =
        (origin == File_Pos_Relative::Beginning) ? std::ios_base::beg :
        (origin == File_Pos_Relative::Current)   ? std::ios_base::cur :
        (origin == File_Pos_Relative::Eof)       ? std::ios_base::end : std::ios_base::end;
    _file.seekg(offset, seek_origin);
    _file.seekp(offset, seek_origin);
}

/*
========================================
*/

bool iofile::eof() const
{
    return (_file.eof() || current_pos() >= file_size());
}

/*
========================================
*/

void iofile::set_mode (
    File_Access new_access_mode, const File_Data new_data_mode) const
{
    if (new_access_mode == _access_mode &&
        new_data_mode == _data_mode)
    {
        return;
    }

    if (new_access_mode == File_Access::Output ||
        new_access_mode == File_Access::Random_Append)
    {
        new_access_mode = File_Access::Append;
    }

    const size_t cur_pos = current_pos();
    close();
    open (new_access_mode, new_data_mode);
    seek (cur_pos);
}


void iofile::set_mode (const File_Access new_access_mode) const
{
    set_mode(new_access_mode, _data_mode);
}


void iofile::set_mode (const File_Data new_data_mode) const
{
    set_mode(_access_mode, new_data_mode);
}

/*
========================================
*/

void iofile::get (
    void* const buffer, const size_t size, const size_t count) const
{
    _file.get((std::fstream::char_type*)buffer, size * count);
}


void iofile::get (void* const buffer, const size_t byte_count) const
{
    get(buffer, 1, byte_count);
}

/*
========================================
*/

std::string iofile::read_string() const
{
    std::string val;
    read_string(val);
    return val;
}


void iofile::read_string (std::string& target_string) const
{
    target_string = "";

    //  Read first four bytes and assume they're the length of the string
    const unsigned int char_count = read_uint();
    target_string = read_string(char_count);
}


std::string iofile::read_string (size_t string_length) const
{
    std::string val;
    read_string(val, string_length);
    return val;
}


void iofile::read_string (
    std::string& target_string, const size_t string_length) const
{
    /*  Read {string_length} bytes from the file into a string. Function doesn't
        care what mode the file was written in; if it was binary and we haven't
        already read in the length, the string's prefix will be read in as four
        regular chars and added to our string.      */

    //  Requested string length longer than file
    if (current_pos() + string_length > file_size()) {
        std::cout <<
            "Error: required string length goes beyond end of file, in file: " <<
            extract_filename(filename()) << " " << intstr(string_length) <<
            " at position: " << current_pos() << std::endl;
        throw core::exception(ERROR_FILE_LINE);
    }

    //  Get char data
    target_string.resize(string_length);
    get(&target_string.front(), string_length);
}

/*
========================================
*/

std::string iofile::line_input() const
{
    std::string val;
    line_input(val);
    return val;
}


void iofile::line_input (std::string& target_string) const
{
//    std::getline(_file, target_string);
//    strip_chars(target_string, "\r\f\n");

    target_string.clear();
    std::istream::sentry  se   (_file, true);
    std::streambuf* const sb = _file.rdbuf();

    for (;;) {
        const int c = sb->sbumpc();
        if (c == '\n') {
            return;
        }
        else if (c == '\r') {
            if (sb->sgetc() == '\n') { sb->sbumpc(); }
            return;
        }
        else if (c == EOF) {
            if (target_string.empty()) {
                _file.setstate(std::ios::eofbit);
            }
            return;
        }
        else {
            target_string += static_cast<char>(c);
        }
    }
}

/*
========================================
*/

void iofile::read_bool (bool& target) const
{
    get(&target, sizeof(bool));
}


bool iofile::read_bool() const
{
    bool value;
    read_bool(value);
    return value;
}

/*
========================================
*/

void iofile::read_ubyte (unsigned char& target) const
{
    get(&target, sizeof(unsigned char));
}


unsigned char iofile::read_ubyte() const
{
    unsigned char value;
    read_ubyte(value);
    return value;
}

/*
========================================
*/

void iofile::read_char (char& target) const
{
    get(&target, sizeof(char));
}


char iofile::read_char() const
{
    char value;
    read_char(value);
    return value;
}

/*
========================================
*/

void iofile::read_byte (char& target) const
{
    get(&target, sizeof(char));
}


char iofile::read_byte() const
{
    return read_char();
}

/*
========================================
*/

void iofile::read_short (short& target) const
{
    get(&target, sizeof(short));

}


short iofile::read_short() const
{
    short value;
    read_short(value);
    return value;
}

/*
========================================
*/

void iofile::read_ushort (unsigned short& target) const
{
    get(&target, sizeof(unsigned short));
}


unsigned short iofile::read_ushort() const
{
    unsigned short value;
    read_ushort(value);
    return value;
}

/*
========================================
*/

void iofile::read_int (int& target) const
{
    get(&target, sizeof(int));
}


int iofile::read_int() const
{
    int value;
    read_int(value);
    return value;
}

/*
========================================
*/

void iofile::read_uint (unsigned int& target) const
{
    get(&target, sizeof(unsigned int));
}

unsigned int iofile::read_uint() const
{
    unsigned int value;
    read_uint(value);
    return value;
}

/*
========================================
*/

void iofile::read_size_t (size_t& target) const
{
    get(&target, sizeof(size_t));
}


size_t iofile::read_size_t() const
{
    size_t value;
    read_size_t(value);
    return value;
}

/*
========================================
*/

void iofile::read_long (long int& target) const
{
    get(&target, sizeof(long int));
}


long int iofile::read_long() const
{
    long int value;
    read_long(value);
    return value;
}

/*
========================================
*/

void iofile::read_ulong (unsigned long int& target) const
{
    get(&target, sizeof(unsigned long int));
}


unsigned long int iofile::read_ulong() const
{
    unsigned long int value;
    read_ulong(value);
    return value;
}

/*
========================================
*/

void iofile::read_float (float& target) const
{
    get(&target, sizeof(float));
}


float iofile::read_float() const
{
    float value;
    read_float(value);
    return value;
}

/*
========================================
*/

void iofile::read_double (double& target) const
{
    get(&target, sizeof(double));
}


double iofile::read_double() const
{
    double value;
    read_double(value);
    return value;
}

/*
========================================
*/

void iofile::put (
    const void* const buffer, const size_t size, const size_t count) const
{
    _file.write((std::fstream::char_type*)buffer, size * count);
}


void iofile::put (const void* const buffer, const size_t byte_count) const
{
    put(buffer, byte_count, 1);
}

/*
========================================
*/

void iofile::open_string_output_buffer (const size_t padding_size /*= 4*/) const
{
    if (_string_output_buffer_on) { throw core::exception(ERROR_FILE_LINE); }
    _string_output_buffer_on  = true;
    _string_output_buffer_pad = padding_size;
    _string_output_buffer.clear();
}

/*
========================================
*/

void iofile::close_string_output_buffer() const
{
    if (!_string_output_buffer_on) { throw core::exception(ERROR_FILE_LINE); }
    _string_output_buffer_on = false;
    write_string(_string_output_buffer);
    _string_output_buffer.clear();
}

/*
========================================
*/

void iofile::write_string (const std::string& str) const
{
    if (_string_output_buffer_on) {
        if (!_string_output_buffer.empty()) {
            _string_output_buffer += std::string(_string_output_buffer_pad, ' ');
        }
        _string_output_buffer += str;
        return;
    }

    if (_data_mode == File_Data::Ascii) {
        write_ascii_string(str);
        return;
    }

    write_size_t(str.size());
    if (!str.empty()) { put(&str.front(), str.size()); }
}

/*
========================================
*/

void iofile::write_naked_string (const std::string& str) const
{
    /*  Writes a naked (untagged) string to file. Normal strings are Pascal-
        style, with an int written first to indicate the string's length.   */

    if (_string_output_buffer_on) {
        write_string(str);
        return;
    }

    if (_data_mode == File_Data::Ascii) {
        write_ascii_string(str);
        return;
    }

    if (!str.empty()) { put(&str.front(), str.size()); }
}

/*
========================================
*/

void iofile::write_ascii_string (const std::string& str) const
{
    if (!str.empty()) { put(&str.front(), str.size()); }
    static const std::string newline = "\n";
    put(&newline.front(), 1);
}

/*
========================================
*/

void iofile::write_bool (const bool value) const
{
    put(&value, sizeof(bool));
}

/*
========================================
*/

void iofile::write_ubyte (const unsigned char value) const
{
    put(&value, sizeof(unsigned char));
}

/*
========================================
*/

void iofile::write_char (const char value) const
{
    put(&value, sizeof(char));
}

/*
========================================
*/

void iofile::write_byte (const char value) const
{
    write_char(value);
}

/*
========================================
*/

void iofile::write_short (const short value) const
{
    put(&value, sizeof(short));
}

/*
========================================
*/

void iofile::write_ushort (const unsigned short value) const
{
    put(&value, sizeof(unsigned short));
}

/*
========================================
*/

void iofile::write_int (const int value) const
{
    put(&value, sizeof(int));
}

/*
========================================
*/

void iofile::write_uint (const unsigned int value) const
{
    put(&value, sizeof(unsigned int));
}

/*
========================================
*/

void iofile::write_size_t (const size_t value) const
{
    put(&value, sizeof(size_t));
}

/*
========================================
*/

void iofile::write_long (const long int value) const
{
    put(&value, sizeof(long int));
}

/*
========================================
*/

void iofile::write_ulong (const unsigned long int value) const
{
    put(&value, sizeof(unsigned long int));
}

/*
========================================
*/

void iofile::write_float (const float value) const
{
    put(&value, sizeof(float));
}

/*
========================================
*/

void iofile::append_file (const std::string& source_filename) const
{
    const iofile source (source_filename, File_Access::Input, File_Data::Binary);
    const size_t  size  = source.file_size();

    if (size > 0) {
        std::vector<unsigned char> buffer(size);
        source.get(&buffer.front(), size);
        put(&buffer.front(), size);
    }
}
