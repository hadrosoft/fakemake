#ifndef HADROSOFT_CORE_FILE_INFO_H_INCLUDED
#define HADROSOFT_CORE_FILE_INFO_H_INCLUDED

#include "file_system.h"

class file_info
{
    public:
        file_info (const file_info& source);
        file_info (const std::string& filename_, File_Type type_, bool read_only_);

        std::string filename() const;
        File_Type   type() const;
        bool        read_only() const;

    private:
        friend class directory_listing;

        std::string _filename;
        File_Type   _type      = File_Type::File;
        bool        _read_only = false;
};

bool operator == (const file_info& lhs, const file_info& rhs);
bool operator < (const file_info& lhs, const file_info& rhs);
bool operator > (const file_info& lhs, const file_info& rhs);

#endif      //  HADROSOFT_CORE_FILE_INFO_H_INCLUDED
