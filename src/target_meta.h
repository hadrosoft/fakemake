#ifndef TARGET_META_H_INCLUDED
#define TARGET_META_H_INCLUDED

#include <string>
#include <vector>

struct SourceDir
{
    bool        recursive;
    std::string directory;
};

struct SourceObjPair
{
    std::string sourceFile;
    std::string objectFile;
};

struct FileSwitch
{
    std::string              filename;
    std::vector<std::string> switches;
};

using SwitchList = std::vector<std::string>;

void fixPath (std::string& path);

#endif      //  TARGET_META_H_INCLUDED
