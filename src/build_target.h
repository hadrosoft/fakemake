#ifndef BUILD_TARGET_H_INCLUDED
#define BUILD_TARGET_H_INCLUDED

#include <vector>
#include <string>

#include "target_meta.h"

struct BuildTarget
{
    std::string                name;
    std::vector<std::string>   buildLines;
    std::string                exeFilename;
    std::vector<std::string>   sourceTypes;
    std::vector<SourceDir>     sourceDirectories;
    std::vector<std::string>   extraSourceFiles;
    std::vector<std::string>   excludeSourceFiles;
    std::vector<SourceObjPair> allSourceFiles;
    std::vector<SourceObjPair> actualBuildList;
    std::vector<std::string>   includeDirectories;
    std::string                objDirectory;
    std::vector<std::string>   compilerSwitches;
    std::vector<std::string>   defines;
    std::vector<std::string>   linkerSwitches;
    std::vector<std::string>   linkerLibraryDirectories;
    std::vector<std::string>   linkerLibraries;
    std::vector<FileSwitch>    fileSwitches;

    void       parse();
    void       getExeFilename (size_t& index);
    void       getSourceFileExtensions (size_t& index);
    void       getSourceDirectories (size_t& index);
    void       getExtraSourceFiles (size_t& index);
    void       getExcludeSourceFiles (size_t& index);
    void       getIncludeDirectories (size_t& index);
    void       getObjDirectory (size_t& index);
    void       getCompilerSwitches (size_t& index);
    void       getPerFileSwitches (size_t& index);
    void       getDefines (size_t& index);
    void       getLinkerSwitches (size_t& index);
    void       getLinkerLibraryDirectories (size_t& index);
    void       getLinkerLibraries (size_t& index);
    void       fixPaths();
    void       compileFinalSourceFileList();
    void       generateObjectFilePaths();
    bool       anyIncludeChanged (const SourceObjPair& sourceFile);
    SwitchList uniqueSwitchesForFile (const std::string& filename);

    void clean();
    bool compile (bool fromClean);
    bool link();
};

extern std::vector<BuildTarget> activeBuildTargets;    //  Targets we're building in this session

BuildTarget* getBuildTarget (const std::string& targetName);

#endif      //  BUILD_TARGET_H_INCLUDED
